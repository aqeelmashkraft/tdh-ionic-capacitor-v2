#!/usr/bin/env bash
LINE="-----------------------------------------------------------------------------------------------"
DLIN="==============================================================================================="
CDATE=$(date '+%Y-%m-%d %H:%M')

echo "git pull"
git pull
echo $LINE

echo "git status"
git status
echo $LINE

echo "git add"
git add . --verbose
echo "git commit"
git commit -m "$CDATE"
echo $LINE

echo "git status"
git status
echo $LINE

echo "git push"
git push
echo $LINE

echo "git status"
git status
echo $DLINE



