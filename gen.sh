#!/usr/bin/env bash

######################################################
# Create a module for the page
# Use lazy loading
######################################################

# ng generate page ActivityDetail


######################################################
# Create a regular Angular component
######################################################

# ng generate component components/build-num --spec false

# ng generate component components/doc-facsimile --spec false
# ng generate component components/doc-facsimile-content --spec false

# ng generate component components/scan-up --spec false

# ng generate component components/scan-error --spec false
# ng generate component components/scan-sim --spec false

# ng generate component components/upload --spec false

#    ng generate component components/doc-specific/tax1041k1 --spec false
#    ng generate component components/doc-specific/tax1065k1 --spec false
#    ng generate component components/doc-specific/tax1095a --spec false
#    ng generate component components/doc-specific/tax1095b --spec false
#    ng generate component components/doc-specific/tax1095c --spec false
#    ng generate component components/doc-specific/tax1097btc --spec false
#    ng generate component components/doc-specific/tax1098 --spec false
#    ng generate component components/doc-specific/tax1098c --spec false
#    ng generate component components/doc-specific/tax1098e --spec false
#    ng generate component components/doc-specific/tax1098t --spec false
#    ng generate component components/doc-specific/tax1099a --spec false
#    ng generate component components/doc-specific/tax1099b --spec false
#    ng generate component components/doc-specific/tax1099c --spec false
#    ng generate component components/doc-specific/tax1099cap --spec false
#    ng generate component components/doc-specific/tax1099div --spec false
#    ng generate component components/doc-specific/tax1099g --spec false
#    ng generate component components/doc-specific/tax1099h --spec false
#    ng generate component components/doc-specific/tax1099int --spec false
#    ng generate component components/doc-specific/tax1099k --spec false
#    ng generate component components/doc-specific/tax1099ltc --spec false
#    ng generate component components/doc-specific/tax1099misc --spec false
#    ng generate component components/doc-specific/tax1099oid --spec false
#    ng generate component components/doc-specific/tax1099patr --spec false
#    ng generate component components/doc-specific/tax1099q --spec false
#    ng generate component components/doc-specific/tax1099r --spec false
#    ng generate component components/doc-specific/tax1099s --spec false
#    ng generate component components/doc-specific/tax1099sa --spec false
#    ng generate component components/doc-specific/tax1120sk1 --spec false
#    ng generate component components/doc-specific/tax2439 --spec false
#    ng generate component components/doc-specific/tax5498 --spec false
#    ng generate component components/doc-specific/tax5498esa --spec false
#    ng generate component components/doc-specific/tax5498sa --spec false
#    ng generate component components/doc-specific/taxw2 --spec false
#    ng generate component components/doc-specific/taxw2g --spec false

#ng generate component components/facsimiles/tax1041k1-facsimile --spec false
#ng generate component components/facsimiles/tax1065k1-facsimile --spec false
#ng generate component components/facsimiles/tax1095a-facsimile --spec false
#ng generate component components/facsimiles/tax1095b-facsimile --spec false
#ng generate component components/facsimiles/tax1095c-facsimile --spec false
#ng generate component components/facsimiles/tax1097btc-facsimile --spec false
#ng generate component components/facsimiles/tax1098-facsimile --spec false
#ng generate component components/facsimiles/tax1098c-facsimile --spec false
#ng generate component components/facsimiles/tax1098e-facsimile --spec false
#ng generate component components/facsimiles/tax1098t-facsimile --spec false
#ng generate component components/facsimiles/tax1099a-facsimile --spec false
#ng generate component components/facsimiles/tax1099b-facsimile --spec false
#ng generate component components/facsimiles/tax1099c-facsimile --spec false
#ng generate component components/facsimiles/tax1099cap-facsimile --spec false
#ng generate component components/facsimiles/tax1099div-facsimile --spec false
#ng generate component components/facsimiles/tax1099g-facsimile --spec false
#ng generate component components/facsimiles/tax1099h-facsimile --spec false
#ng generate component components/facsimiles/tax1099int-facsimile --spec false
#ng generate component components/facsimiles/tax1099k-facsimile --spec false
#ng generate component components/facsimiles/tax1099ltc-facsimile --spec false
#ng generate component components/facsimiles/tax1099misc-facsimile --spec false
#ng generate component components/facsimiles/tax1099oid-facsimile --spec false
#ng generate component components/facsimiles/tax1099patr-facsimile --spec false
#ng generate component components/facsimiles/tax1099q-facsimile --spec false
#ng generate component components/facsimiles/tax1099r-facsimile --spec false
#ng generate component components/facsimiles/tax1099s-facsimile --spec false
#ng generate component components/facsimiles/tax1099sa-facsimile --spec false
#ng generate component components/facsimiles/tax1120sk1-facsimile --spec false
#ng generate component components/facsimiles/tax2439-facsimile --spec false
#ng generate component components/facsimiles/tax5498-facsimile --spec false
#ng generate component components/facsimiles/tax5498esa-facsimile --spec false
#ng generate component components/facsimiles/tax5498sa-facsimile --spec false
#ng generate component components/facsimiles/taxw2-facsimile --spec false
#ng generate component components/facsimiles/taxw2g-facsimile --spec false
