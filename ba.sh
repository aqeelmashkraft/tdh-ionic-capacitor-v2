#!/usr/bin/env bash

BUILDNUM=$(date '+%Y-%m-%d %H:%M')
BUILDSPAN="<span>$BUILDNUM</span>"
echo $BUILDSPAN > /Users/brucewilcox/dev/ionic4/tdh-ionic-capacitor-v1/src/app/components/build-num/build-num.component.html

ionic build

npx cap copy

npx cap open android

