import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { StatusBar } from '@ionic-native/status-bar/ngx';

import { GlobalDataService } from './services/global-data.service';
import {LoggerService} from './services/logger.service';

// import { Plugins } from '@capacitor/core';
// const { SplashScreen } = Plugins;
// Hide the splash (you should do this on app launch)
// SplashScreen.hide( );


@Component( {
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: [ 'app.component.scss' ]
} )
export class AppComponent {

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private global: GlobalDataService,
        private logger: LoggerService
    ) {

        this.initializeApp( );

    }

    initializeApp( ) {

        this.logger.trace( `AppComponent initializeApp` );

        this.platform
            .ready( )
            .then(
                ( ) => {

                    this.logger.trace( `AppComponent platform ready` );

                    this.splashScreen.hide( );

                    this.statusBar.styleDefault( );

                    this.global.init( );

                }
            );

    }

}
