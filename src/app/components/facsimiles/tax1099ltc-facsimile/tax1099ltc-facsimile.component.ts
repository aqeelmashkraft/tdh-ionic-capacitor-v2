import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Ltc } from '../../../models/tax1099Ltc';


@Component({
    selector: 'app-tax1099ltc-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099ltc.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099LtcFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 454;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 454 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099Ltc;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 17 );

        // 1  PAYER'S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.payerNameAddress ), 8, 83 );

        // 2  PAYER'S TIN
        this.placeTextCenter( this.doc.payerTin, 122, 211 );

        // 3  POLICYHOLDER'S TIN
        this.placeTextCenter( this.doc.policyholderTin, 367, 211 );

        // 4  POLICYHOLDER'S name
        this.placeParagraph( this.formatter.format( 'Name', this.doc.policyHolderNameAddress ), 8, 255 );

        // 5  POLICYHOLDER'S street address (including apt. no.)
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.policyHolderNameAddress ), 8, 331 );

        // 6  POLICYHOLDER'S city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.policyHolderNameAddress ), 8, 379 );

        // 7  Account number
        this.placeTextCenter( this.doc.accountNumber, 165, 427 );

        // 8 1 Gross long-term care benefits paid
        this.placeTextRight( this.formatter.format( 'number', this.doc.ltcBenefits ), 685, 115 );

        // 9 2 Accelerated death benefits paid
        this.placeTextRight( this.formatter.format( 'number', this.doc.deathBenefits ), 685, 187 );

        // 10  Per diem
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.perDiem ), 510, 227 );

        // 11  Reimbursed amount
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.reimbursedAmount ), 582, 227 );

        // 12  INSURED'S taxpayer identification no.
        this.placeTextCenter( this.doc.insuredId, 790, 235 );

        // 13  INSURED'S name
        this.placeText( this.formatter.format( 'Name', this.doc.insuredNameAddress ), 497, 283 );

        // 14  INSURED's street address (including apt. no.)
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.insuredNameAddress ), 497, 331 );

        // 15  INSURED'S city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.insuredNameAddress ), 497, 379 );

        // 16  Qualified contract
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.qualifiedContract ), 353, 423 );

        // 17  Chronically ill
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.chronicallyIll ), 668, 399 );

        // 18  Terminally ill
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.terminallyIll ), 668, 423 );

        // 19  Date certified (MM/DD/YY)
        this.placeTextCenter( this.formatter.format( 'Timestamp', this.doc.dateCertified ), 833, 427 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}