import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Patr } from '../../../models/tax1099Patr';


@Component({
    selector: 'app-tax1099patr-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099ptr.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099PatrFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 454;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 454 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099Patr;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 17 );

        // 1  PAYER'S name, street address, city or town, state or province, country, ZIP
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.payerNameAddress ), 8, 83 );

        // 2  PAYER'S TIN
        this.placeTextCenter( this.doc.payerTin, 120, 211 );

        // 3  RECIPIENT'S TIN
        this.placeTextCenter( this.doc.recipientTin, 365, 211 );

        // 4  RECIPIENT'S name
        this.placeParagraph( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 255 );

        // 5  RECIPIENT'S street address (including apt. no.)
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 331 );

        // 6  RECIPIENT'S city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 379 );

        // 7  Account number
        this.placeTextCenter( this.doc.accountNumber, 243, 427 );

        // 8 1 Patronage dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.patronageDividends ), 687, 67 );

        // 9 2 Nonpatronage distributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.nonpatronageDistributions ), 687, 115 );

        // 10 3 Per-unit retain allocations
        this.placeTextRight( this.formatter.format( 'number', this.doc.perUnitRetainAllocations ), 687, 163 );

        // 11 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 687, 211 );

        // 12 5 Redemption of nonqualified notices and retain allocations
        this.placeTextRight( this.formatter.format( 'number', this.doc.redemption ), 687, 283 );

        // 13 6 Domestic production activities deduction
        this.placeTextRight( this.formatter.format( 'number', this.doc.dpaDeduction ), 889, 283 );

        // 14 7 Qualified payments
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedPayments ), 687, 331 );

        // 15 8 Investment credit
        this.placeTextRight( this.formatter.format( 'number', this.doc.investmentCredit ), 889, 331 );

        // 16 9 Work opportunity credit
        this.placeTextRight( this.formatter.format( 'number', this.doc.workOpportunityCredit ), 687, 379 );

        // 17 10 Patron's AMT adjustment
        this.placeTextRight( this.formatter.format( 'number', this.doc.patronsAmtAdjustment ), 889, 379 );

        // 18 11 Other credits and deductions
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherCreditsAndDeductions ), 889, 427 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}