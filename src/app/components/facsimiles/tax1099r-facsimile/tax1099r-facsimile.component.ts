import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099R } from '../../../models/tax1099R';


@Component({
    selector: 'app-tax1099r-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099r.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099RFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 695;

    trimWidth: number = 1053;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 695 / 1053;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099R;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1053;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0 $field.boxNumber $field.boxDescription
        this.placeText( '', 395, 17 );

        // 1  PAYER's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.payerNameAddress ), 8, 91 );

        // 2  PAYER's TIN
        this.placeTextCenter( this.doc.payerTin, 120, 307 );

        // 3  RECIPIENT's TIN
        this.placeTextCenter( this.doc.recipientTin, 365, 307 );

        // 4  RECIPIENT's name
        this.placeParagraph( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 355 );

        // 5  RECIPIENT's street address
        this.placeParagraph( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 449 );

        // 6  RECIPIENT's city, state, and ZIP code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 523 );

        // 7 10 Amount allocable to IRR within 5 years
        this.placeTextRight( this.formatter.format( 'number', this.doc.allocableToIRR ), 241, 595 );

        // 8 11 First year of designated Roth
        this.placeTextCenter( this.formatter.format( 'integer', this.doc.firstYearOfRoth ), 307, 595 );

        // 9  FATCA filing requirement
        this.placeText( this.formatter.format( 'Boolean', this.doc.foreignAccountTaxCompliance ), 426, 590 );

        // 10  Account number
        this.placeTextCenter( this.doc.recipientAccountNumber, 185, 667 );

        // 11  Date of payment
        this.placeText( this.formatter.format( 'Timestamp', this.doc.dateOfPayment ), 378, 667 );

        // 12 1 Gross distribution
        this.placeTextRight( this.formatter.format( 'number', this.doc.grossDistribution ), 687, 91 );

        // 13 2a Taxable amount
        this.placeTextRight( this.formatter.format( 'number', this.doc.taxableAmount ), 687, 163 );

        // 14 2b Taxable amount not determined
        this.placeText( this.formatter.format( 'Boolean', this.doc.taxableAmountNotDetermined ), 657, 207 );

        // 15 2c Total distribution
        this.placeText( this.formatter.format( 'Boolean', this.doc.totalDistribution ), 858, 206 );

        // 16 3 Capital gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.capitalGain ), 687, 307 );

        // 17 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 889, 307 );

        // 18 5 Employee contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.employeeContributions ), 687, 403 );

        // 19 6 Net unrealized appreciation
        this.placeTextRight( this.formatter.format( 'number', this.doc.netUnrealizedAppreciation ), 889, 403 );

        // 20 7a Distribution code(s)
        this.placeTextCenter( this.formatter.format( 'Concat', this.doc.distributionCodes ), 559, 475 );

        // 21 7b IRA/SEP/SIMPLE
        this.placeText( this.formatter.format( 'Boolean', this.doc.iraSepSimple ), 657, 471 );

        // 22 8a Other
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherAmount ), 831, 475 );

        // 23 8b Other percent
        this.placeText( this.formatter.format( 'number', this.doc.otherPercent ), 839, 475 );

        // 24 9a Your percent of total distribution
        this.placeText( this.formatter.format( 'number', this.doc.yourPercentOfTotal ), 609, 523 );

        // 25 9b Total employee contributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.totalEmployeeContributions ), 889, 523 );

        // 26 12 State tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 687, 571 );

        // 28 13 State/PAYER's state number
        this.placeTextCenter( this.formatter.format3( 'StateCodeId', this.doc.stateTaxWithholding, 1 ), 790, 571 );

        // 30 14 State distribution
        this.placeTextRight( this.formatter.format3( 'StateIncome', this.doc.stateTaxWithholding, 1 ), 1049, 571 );

        // 32 15 Local tax withheld
        this.placeTextRight( this.formatter.format3( 'LocalWH', this.doc.localTaxWithholding, 1 ), 687, 643 );

        // 34 16 Name of locality
        this.placeTextCenter( this.formatter.format3( 'Locality', this.doc.localTaxWithholding, 1 ), 791, 643 );

        // 36 17 Local distribution
        this.placeTextRight( this.formatter.format3( 'LocalIncome', this.doc.localTaxWithholding, 1 ), 1049, 643 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}