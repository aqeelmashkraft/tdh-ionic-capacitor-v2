import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Misc } from '../../../models/tax1099Misc';


@Component({
    selector: 'app-tax1099misc-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099msc.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099MiscFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 694;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 694 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099Misc;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 17 );

        // 1  Payer's name, address, city, state and ZIP code
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.payerNameAddress ), 8, 91 );

        // 2  Payer's identification number
        this.placeTextCenter( this.doc.payerTin, 120, 307 );

        // 3  Recipient's identification number
        this.placeTextCenter( this.doc.recipientTin, 365, 307 );

        // 4  Recipient's name
        this.placeText( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 379 );

        // 5  Recipient's street address
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 451 );

        // 6  Recipient's city, state, and ZIP code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 523 );

        // 7  Account number
        this.placeTextCenter( this.doc.accountNumber, 156, 595 );

        // 8  FATCA filing requirement
        this.placeText( this.formatter.format( 'Boolean', this.doc.foreignAccountTaxCompliance ), 357, 583 );

        // 9 1 Rents
        this.placeTextRight( this.formatter.format( 'number', this.doc.rents ), 685, 91 );

        // 10 2 Royalties
        this.placeTextRight( this.formatter.format( 'number', this.doc.royalties ), 685, 163 );

        // 11 3 Other income
        this.placeTextRight( this.formatter.format( 'number', this.doc.otherIncome ), 685, 211 );

        // 12 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 887, 211 );

        // 13 5 Fishing boat proceeds
        this.placeTextRight( this.formatter.format( 'number', this.doc.fishingBoatProceeds ), 685, 307 );

        // 14 6 Medical and health care payments
        this.placeTextRight( this.formatter.format( 'number', this.doc.medicalHealthPayment ), 887, 307 );

        // 15 7 Nonemployee compensation
        this.placeTextRight( this.formatter.format( 'number', this.doc.nonEmployeeCompensation ), 685, 403 );

        // 16 8 Substitute payments in lieu of dividends or interest
        this.placeTextRight( this.formatter.format( 'number', this.doc.substitutePayments ), 887, 403 );

        // 17 9 Payer made direct sales of $5,000 or more
        this.placeText( this.formatter.format( 'Boolean', this.doc.payerDirectSales ), 670, 469 );

        // 18 10 Crop insurance proceeds
        this.placeTextRight( this.formatter.format( 'number', this.doc.cropInsurance ), 887, 475 );

        // 21 13 Excess golden parachute payments
        this.placeTextRight( this.formatter.format( 'number', this.doc.excessGolden ), 685, 595 );

        // 22 14 Gross proceeds paid to an attorney
        this.placeTextRight( this.formatter.format( 'number', this.doc.grossAttorney ), 887, 595 );

        // 23 15a Section 409A deferrals
        this.placeTextRight( this.formatter.format( 'number', this.doc.section409ADeferrals ), 239, 667 );

        // 24 15b Section 409A income
        this.placeTextRight( this.formatter.format( 'number', this.doc.section409AIncome ), 483, 667 );

        // 25 16 State tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 685, 643 );

        // 27 17 State / Payer's state number
        this.placeTextCenter( this.formatter.format3( 'StateCodeId', this.doc.stateTaxWithholding, 1 ), 790, 643 );

        // 29 18 State income
        this.placeTextRight( this.formatter.format3( 'StateIncome', this.doc.stateTaxWithholding, 1 ), 1049, 643 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}