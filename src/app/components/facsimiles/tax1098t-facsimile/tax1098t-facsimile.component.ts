import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1098T } from '../../../models/tax1098T';


@Component({
    selector: 'app-tax1098t-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1098t.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1098TFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 458;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 458 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1098T;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED
        this.placeText( '', 393, 17 );

        // 1  Filer's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.filerNameAddress ), 6, 83 );

        // 2  Filer's federal identification number
        this.placeTextCenter( this.doc.filerTin, 120, 211 );

        // 3  Student's social security number
        this.placeTextCenter( this.doc.studentTin, 365, 211 );

        // 4  Student's name
        this.placeText( this.formatter.format( 'Name', this.doc.studentNameAddress ), 6, 283 );

        // 5  Student's address
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.studentNameAddress ), 6, 331 );

        // 6  Student's city, state, and ZIP code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.studentNameAddress ), 6, 379 );

        // 7  Account number
        this.placeTextCenter( this.doc.accountNumber, 150, 427 );

        // 8 8 Check if at least half-time student
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.halfTime ), 471, 419 );

        // 9 1 Payments received for qualified tuition and related expenses
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedTuitionFees ), 687, 91 );

        // 10 4 Adjustments made for a prior year
        this.placeTextRight( this.formatter.format( 'number', this.doc.adjustmentPriorYear ), 687, 283 );

        // 11 5 Scholarships or grants
        this.placeTextRight( this.formatter.format( 'number', this.doc.scholarship ), 889, 283 );

        // 12 6 Adjustments to scholarships or grants for a prior year
        this.placeTextRight( this.formatter.format( 'number', this.doc.adjustScholarship ), 687, 379 );

        // 13 7 Check if the amount in box 1 or box 2 includes amounts for an academic period beginning January - March 2018
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.includeJanMar ), 874, 375 );

        // 14 9 Check if graduate student
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.graduate ), 672, 419 );

        // 15 10 Insurance contract reimbursement / refund
        this.placeTextRight( this.formatter.format( 'number', this.doc.insuranceRefund ), 889, 427 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}