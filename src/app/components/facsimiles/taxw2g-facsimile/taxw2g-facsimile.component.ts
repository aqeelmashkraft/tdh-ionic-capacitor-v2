import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { TaxW2G } from '../../../models/taxW2G';


@Component({
    selector: 'app-taxw2g-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/fw2g.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class TaxW2GFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 693;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 693 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: TaxW2G;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 395, 16 );

        // 1  PAYER'S name, street address, city or town, province or state, country, andZIP or foreign postal code
        this.placeParagraph( this.formatter.format( 'NameAddress', this.doc.payerNameAddress ), 8, 91 );

        // 2  PAYER'S federal identification number
        this.placeTextCenter( this.doc.payerTin, 120, 307 );

        // 3  PAYER'S telephone number
        this.placeTextCenter( this.formatter.format( 'Phone', this.doc.payerNameAddress ), 365, 307 );

        // 4  WINNER'S name
        this.placeParagraph( this.formatter.format( 'Name', this.doc.winnerNameAddress ), 8, 351 );

        // 5  WINNER'S street address (including apt. no.)
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.winnerNameAddress ), 8, 451 );

        // 6  WINNER'S city or town, province or state, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.winnerNameAddress ), 8, 523 );

        // 7 1 Reportable winnings
        this.placeTextRight( this.formatter.format( 'number', this.doc.winnings ), 685, 91 );

        // 8 2 Date won
        this.placeTextCenter( this.formatter.format( 'Timestamp', this.doc.dateWon ), 791, 91 );

        // 9 3 Type of wager
        this.placeTextCenter( this.doc.typeOfWager, 589, 139 );

        // 10 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 887, 139 );

        // 11 5 Transaction
        this.placeTextCenter( this.doc.transaction, 589, 187 );

        // 12 6 Race
        this.placeTextCenter( this.doc.race, 791, 187 );

        // 13 7 Winnings from identical wagers
        this.placeTextRight( this.formatter.format( 'number', this.doc.identicalWinnings ), 685, 235 );

        // 14 8 Cashier
        this.placeTextCenter( this.doc.cashier, 791, 235 );

        // 15 9 Winner's taxpayer identification no.
        this.placeTextCenter( this.doc.winnerTin, 588, 307 );

        // 16 10 Window
        this.placeTextCenter( this.doc.window, 791, 307 );

        // 17 11 First I.D.
        this.placeTextCenter( this.doc.firstId, 588, 379 );

        // 18 12 Second I.D.
        this.placeTextCenter( this.doc.secondId, 790, 379 );

        // 19 13 State/PAYER's state identification no.
        this.placeTextCenter( this.formatter.format4( 'StateStateId', this.doc.payerState, this.doc.payerStateId, ' ' ), 588, 451 );

        // 20 14 State winnings
        this.placeTextRight( this.formatter.format( 'number', this.doc.stateWinnings ), 887, 451 );

        // 21 15 State income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.stateTaxWithheld ), 685, 523 );

        // 22 16 Local winnings
        this.placeTextRight( this.formatter.format( 'number', this.doc.localWinnings ), 887, 523 );

        // 23 17 Local income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.localTaxWithheld ), 685, 595 );

        // 24 18 Name of locality
        this.placeText( this.doc.localityName, 697, 595 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}