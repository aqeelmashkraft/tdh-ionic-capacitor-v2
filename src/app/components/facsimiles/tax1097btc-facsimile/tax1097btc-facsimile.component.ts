import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1097Btc } from '../../../models/tax1097Btc';


@Component({
    selector: 'app-tax1097btc-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1097btc.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1097BtcFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 695;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 695 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1097Btc;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 16 );

        // 1  FORM 1097-BTC ISSUER'S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.issuerNameAddress ), 8, 91 );

        // 2  FORM 1097-BTC ISSUER'S TIN
        this.placeTextCenter( this.doc.issuerTin, 120, 283 );

        // 3  RECIPIENT'S TIN
        this.placeTextCenter( this.doc.recipientTin, 365, 283 );

        // 4  RECIPIENT'S name
        this.placeText( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 355 );

        // 5  RECIPIENT'S street address (including apt. no.)
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 427 );

        // 6  RECIPIENT'S city or town, state or province, country, and ZIP or foreign postal code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 499 );

        // 7  Form 1097-BTC issuer is: Issuer of bond or its agent filing 2019 Form 1097-BTC for credit being reported
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.filingForCredit ), 29, 567 );

        // 8  Form 1097-BTC issuer is: An entity or a person that received or should have received a 2019 Form 1097-BTC and is distributing part or all of that credit to others
        this.placeTextCenter( this.formatter.format( 'Boolean', this.doc.asNominee ), 29, 615 );

        // 9 1 Total
        this.placeTextRight( this.formatter.format( 'number', this.doc.total ), 687, 91 );

        // 10 2a Code
        this.placeTextCenter( this.doc.bondCode, 588, 163 );

        // 11 2b Unique Identifier
        this.placeTextCenter( this.doc.uniqueId, 689, 235 );

        // 12 3 Bond type
        this.placeTextCenter( this.doc.bondType, 588, 307 );

        // 13 5a January
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'JAN' ), 687, 355 );

        // 14 5b February
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'FEB' ), 889, 355 );

        // 15 5c March
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'MAR' ), 687, 403 );

        // 16 5d April
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'APR' ), 889, 403 );

        // 17 5e May
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'MAY' ), 687, 451 );

        // 18 5f June
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'JUN' ), 889, 451 );

        // 19 5g July
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'JUL' ), 687, 499 );

        // 20 5h August
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'AUG' ), 889, 499 );

        // 21 5i September
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'SEP' ), 687, 547 );

        // 22 5j October
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'OCT' ), 889, 547 );

        // 23 5k November
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'NOV' ), 687, 595 );

        // 24 5l December
        this.placeTextRight( this.formatter.format3( 'MonthAmount', this.doc.amounts, 'DEC' ), 889, 595 );

        // 25 6 Comments
        this.placeParagraph( this.doc.comments, 495, 643 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}