import { Component, Input, OnInit, HostListener, AfterViewInit, AfterViewChecked, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Div } from '../../../models/tax1099Div';


@Component({
    selector: 'app-tax1099div-facsimile',
    styles: [
        '#formCanvas { background: url("./assets/img/f1099div.recipient.png"); background-size: cover }',
        '#formDiv { background-color: white; overflow-x: scroll; }',
    ],
    template: `
<div #formDiv id="formDiv" width="100%">
     <canvas #formCanvas id="formCanvas" width="100" height="200">
     </canvas>
</div>
`
})
export class Tax1099DivFacsimileComponent implements OnInit, AfterViewInit, AfterViewChecked, AfterContentInit {

    viewCheckedCount: number = 0;

    trimHeight: number = 693;

    trimWidth: number = 1054;

    minWidth: number = Math.round( this.trimWidth * .6 );

    aspectFraction: number = 693 / 1054;

    adjFraction: number = 1;

    divWidth: number;

    canvasHeight: number;

    canvasWidth: number;

    context: CanvasRenderingContext2D;

    @Input()
    public doc: Tax1099Div;

    @ViewChild(
        'formDiv',
        { static: false,
          read: ElementRef }
    )
    public formDivView: ElementRef;

    @ViewChild(
        'formCanvas',
        { static: false }
    )
    public formCanvasView: ElementRef;

    @HostListener( 'window:resize', ['$event'] )
    onResize( event ) {
        console.log('onResize');
        this.redraw( );
    }

    constructor(
        public formatter: FormatterService
    ) {
        console.log('constructor');
        this.viewCheckedCount = 0;
    }


    ngOnInit() {

        console.log('ngOnInit');

        // this.redraw( );

    }

    ngAfterViewInit() {

        console.log('ngAfterViewInit');

        // this.redraw();

    }

    ngAfterViewChecked() {

        this.viewCheckedCount += 1;

        console.log( `ngAfterViewChecked ${this.viewCheckedCount}` );

        this.redraw();

    }

    ngAfterContentInit() {

        console.log('ngAfterContentInit');

        // this.redraw( );

    }

    ionViewWillEnter() {

        console.log('ionViewWillEnter');

        // this.redraw();

    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter');

        // this.redraw();

    }

    redraw() {

        console.log('redraw');

        console.log(this.formDivView);
        const divElement = this.formDivView.nativeElement;
        console.log(divElement);

        console.log(this.formCanvasView);
        const canvasElement = this.formCanvasView.nativeElement;
        console.log(canvasElement);

        // Get surrounding div dimensions
        this.divWidth = divElement.clientWidth;
        if (this.divWidth == 0) {
            this.divWidth = 300; // temp
        }
        if ( this.divWidth < this.minWidth ) {
            this.divWidth = this.minWidth;
        }
        console.log('divWidth:' + this.divWidth);

        // Get canvas original dimensions
        this.canvasWidth = canvasElement.width;
        console.log('original canvasWidth:' + this.canvasWidth);
        this.canvasHeight = canvasElement.height;
        console.log('original canvasHeight:' + this.canvasHeight);

        console.log(canvasElement.parentNode.clientWidth);

        this.initCanvas( this.divWidth );

        this.placeData( );

    }

    initCanvas(
        newWidth: number
    ) {

        const canvasElement = this.formCanvasView.nativeElement;

        // Reset canvas width
        canvasElement.width = newWidth;
        this.canvasWidth = canvasElement.width;
        console.log('new canvasWidth:' + this.canvasWidth);

        // Reset canvas height, maintain aspect ratio
        canvasElement.height = canvasElement.width * this.aspectFraction;
        this.canvasHeight = canvasElement.height;
        console.log('new canvasHeight:' + this.canvasHeight);

        // Compute adjustment fraction
        this.adjFraction = this.canvasWidth / 1054;
        console.log('adjFraction:' + this.adjFraction)

        // Get context
        this.context = canvasElement.getContext('2d');

        // Reset font size
        const fontSize: number = Math.floor( 18 * this.adjFraction );
        this.context.font = '' + fontSize + 'px Arial';
        console.log( 'context.font:' + this.context.font );

        this.context.fillStyle = 'blue';
        this.context.strokeStyle = 'blue';

    }

    placeData( ) {

        console.log( 'placeData' );

        // 0  CORRECTED (if checked)
        this.placeText( '', 393, 15 );

        // 1  Payer's name, address, city, state, and ZIP code
        this.placeParagraph( this.formatter.format( 'NameEtc', this.doc.payerNameAddress ), 8, 83 );

        // 2  Payer's TIN
        this.placeTextCenter( this.doc.payerTin, 120, 307 );

        // 3  Recipient's TIN
        this.placeTextCenter( this.doc.recipientTin, 365, 307 );

        // 4  Recipient's name
        this.placeText( this.formatter.format( 'Name', this.doc.recipientNameAddress ), 8, 379 );

        // 5  Recipient's address
        this.placeText( this.formatter.format( 'StreetAddress', this.doc.recipientNameAddress ), 8, 451 );

        // 6  Recipient's city, state, and ZIP code
        this.placeText( this.formatter.format( 'CityStateZip', this.doc.recipientNameAddress ), 8, 523 );

        // 7  FATCA filing requirement
        this.placeText( this.formatter.format( 'Boolean', this.doc.foreignAccountTaxCompliance ), 442, 586 );

        // 8  Account number
        this.placeTextCenter( this.doc.accountNumber, 243, 667 );

        // 9 1a Total ordinary dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.ordinaryDividends ), 687, 91 );

        // 10 1b Qualified dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.qualifiedDividends ), 687, 163 );

        // 11 2a Total capital gain distributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.totalCapitalGain ), 687, 211 );

        // 12 2b Unrecaptured Section 1250 gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.unrecaptured1250Gain ), 889, 211 );

        // 13 2c Section 1202 gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.section1202Gain ), 687, 307 );

        // 14 2d Collectibles (28%) gain
        this.placeTextRight( this.formatter.format( 'number', this.doc.collectiblesGain ), 889, 307 );

        // 15 3 Nondividend distributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.nonTaxableDistribution ), 687, 355 );

        // 16 4 Federal income tax withheld
        this.placeTextRight( this.formatter.format( 'number', this.doc.federalTaxWithheld ), 889, 355 );

        // 17 5 Section 199A dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.section199ADividends ), 687, 403 );

        // 18 6 Investment expenses
        this.placeTextRight( this.formatter.format( 'number', this.doc.investmentExpenses ), 889, 403 );

        // 19 7 Foreign tax paid
        this.placeTextRight( this.formatter.format( 'number', this.doc.foreignTaxPaid ), 687, 475 );

        // 20 8 Foreign country or U.S. possession
        this.placeTextCenter( this.doc.foreignCountry, 792, 475 );

        // 21 9 Cash liquidation distributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.cashLiquidation ), 687, 523 );

        // 22 10 Noncash liquidation distributions
        this.placeTextRight( this.formatter.format( 'number', this.doc.nonCashLiquidation ), 889, 523 );

        // 23 11 Exempt-interest dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.taxExemptInterestDividend ), 687, 595 );

        // 24 12 Specified private activity bond interest dividends
        this.placeTextRight( this.formatter.format( 'number', this.doc.specifiedPabInterestDividend ), 889, 595 );

        // 25 13 State
        this.placeTextCenter( this.formatter.format3( 'StateCode', this.doc.stateTaxWithholding, 1 ), 523, 643 );

        // 27 14 State identification number
        this.placeTextCenter( this.formatter.format3( 'StateId', this.doc.stateTaxWithholding, 1 ), 624, 643 );

        // 29 15 State tax withheld
        this.placeTextRight( this.formatter.format3( 'StateWH', this.doc.stateTaxWithholding, 1 ), 889, 643 );



    }

    placeParagraph(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        const lines: string[] = text.split( '\n' );

        for ( let line of lines ) {
            this.context.textAlign = 'left';
            this.context.fillText( line, x * this.adjFraction, y * this.adjFraction );
            y = y + ( 24 );
        }

    }

    placeText(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'left';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextRight(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'right';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeTextCenter(
        text: string,
        x: number,
        y: number
    ) {

        if ( ! text ) {
            return;
        }

        this.context.textAlign = 'center';
        this.context.fillText(
            text,
            x * this.adjFraction,
            y * this.adjFraction
        );

    }

    placeRectangle(
        x: number,
        y: number,
        width: number,
        height: number
    ) {

        this.context.beginPath( );
        this.context.rect( x, y, width, height );
        this.context.stroke( );

    }

}