import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';

@Component( {
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: [ './welcome.component.scss' ],
} )
export class WelcomeComponent implements OnInit {

    constructor( private global: GlobalDataService ) {
    }

    ngOnInit() {
        // this.global.initializeStorage( );
    }

}
