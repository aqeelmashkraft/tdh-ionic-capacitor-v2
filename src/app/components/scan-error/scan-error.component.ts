import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalDataService } from '../../services/global-data.service';
import {LoggerService} from '../../services/logger.service';

@Component( {
    selector: 'app-scan-error',
    templateUrl: './scan-error.component.html',
    styleUrls: [ './scan-error.component.scss' ],
} )
export class ScanErrorComponent implements OnInit {

    public errorCode: number;

    constructor(
        private global: GlobalDataService,
        private logger: LoggerService,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit( ) {

        this.logger.trace( 'ScanErrorComponent ngOnInit' );

        this.route
            .queryParams
            .subscribe(
                ( params ) => {
                    this.errorCode = Number( params[ 'errorCode' ] );
                }
            );

    }

}
