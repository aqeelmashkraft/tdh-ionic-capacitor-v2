import { Component, Input, OnInit } from '@angular/core';
import { TaxDocListItem } from '../../models/TaxDocListItem';
import { GlobalDataService } from '../../services/global-data.service';
import {LoggerService} from '../../services/logger.service';

@Component( {
    selector: 'app-doc-facsimile-content',
    templateUrl: './doc-facsimile-content.component.html',
    styleUrls: [ './doc-facsimile-content.component.scss' ],
} )
export class DocFacsimileContentComponent implements OnInit {

    @Input()
    public docData: TaxDocListItem;

    public docId: string;

    constructor(
        private global: GlobalDataService,
        private logger: LoggerService
    ) {

    }

    ngOnInit() {

        this.logger.trace( 'DocFacsimileContentComponent ngOnInit begin' );

        this.docId = this.docData.id;

    }

}
