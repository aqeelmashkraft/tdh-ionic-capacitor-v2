import {Component, OnInit} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {GlobalDataService} from '../../services/global-data.service';
import {TaxData} from '../../models/taxData';
import {LoggerService} from '../../services/logger.service';

@Component({
    selector: 'app-scan-sim',
    templateUrl: './scan-sim.component.html',
    styleUrls: ['./scan-sim.component.scss'],
})
export class ScanSimComponent implements OnInit {

    constructor(
        private router: Router,
        private global: GlobalDataService,
        private logger: LoggerService
    ) {
    }

    ngOnInit() {

    }

    simulate( dodNumber: number ) {

        const taxData: TaxData
            = this.global.getRandomDocGivenNumber( dodNumber );

        const dummyData: string = JSON.stringify(taxData, null, 2 );

        const docId: number = this.global.recordScannedText(
            dummyData
        );

        if (docId > -1) {

            this.goToSuccessScreen(docId);

        } else {

            this.goToFailureScreen(docId);

        }

    }

    scanQrCodeDummySuccess() {

        this.logger.trace('ScanDocComponent scanQrCodeDummySuccess');

        const dummyData: string
            = this.global.getRandomDocJson();

        const docId: number = this.global.recordScannedText(
            dummyData
        );

        if (docId > -1) {

            this.goToSuccessScreen(docId);

        } else {

            this.goToFailureScreen(docId);

        }

    }

    scanQrCodeDummyFailure() {

        this.logger.trace('ScanDocComponent scanQrCodeDummyFailure');

        this.global.lastScannedText = 'Junk scan';

        this.goToFailureScreen(this.global.PARSE_ERROR);

    }

    goToSuccessScreen(
        docId: number
    ) {

        this.logger.trace(`ScanDocComponent goToSuccessScreen ${docId}`);

        // this.router.navigate(
        //     [ '/', 'doc-facsimile', docId ]
        // );

        this.router.navigate(
            ['/', 'docs']
        );

    }

    goToFailureScreen(
        errorCode: number
    ) {

        this.logger.trace('ScanDocComponent goToFailureScreen');

        const navigationExtras: NavigationExtras = {
            queryParams: {'errorCode': errorCode}
        };

        this.router.navigate(
            ['/', 'scan-error'],
            navigationExtras
        );

    }

}
