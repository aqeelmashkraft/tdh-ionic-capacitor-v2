import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1097Btc } from '../../../models/tax1097Btc';


@Component({
    selector: 'app-tax1097btc',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.issuerNameAddress">
    <ion-label>Issuer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.issuerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.issuerTin">
    <ion-label>FORM 1097-BTC ISSUER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.issuerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>RECIPIENT'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.filingForCredit">
    <ion-label>Form 1097-BTC issuer is: Issuer of bond or its agent filing 2019 Form 1097-BTC for credit being reported</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.filingForCredit ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.asNominee">
    <ion-label>Form 1097-BTC issuer is: An entity or a person that received or should have received a 2019 Form 1097-BTC and is distributing part or all of that credit to others</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.asNominee ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.total">
    <ion-label>Total</ion-label>
    <ion-chip color="primary">{{ doc.total }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bondCode">
    <ion-label>Code</ion-label>
    <ion-chip color="primary">{{ doc.bondCode }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.uniqueId">
    <ion-label>Unique Identifier</ion-label>
    <ion-chip color="primary">{{ doc.uniqueId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bondType">
    <ion-label>Bond type</ion-label>
    <ion-chip color="primary">{{ doc.bondType }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.amounts">
    <ion-label>Amounts by month</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.amounts">
    <ion-chip slot="end" color="primary"> {{ o.month }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.comments">
    <ion-label>Comments</ion-label>
    <ion-chip color="primary">{{ doc.comments }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1097BtcComponent implements OnInit {

    @Input()
    public doc: Tax1097Btc;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
