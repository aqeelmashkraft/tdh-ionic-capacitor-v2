import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Misc } from '../../../models/tax1099Misc';


@Component({
    selector: 'app-tax1099misc',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>Payer's identification number</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>Recipient's identification number</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignAccountTaxCompliance">
    <ion-label>FATCA filing requirement</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreignAccountTaxCompliance ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rents">
    <ion-label>Rents</ion-label>
    <ion-chip color="primary">{{ doc.rents }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.royalties">
    <ion-label>Royalties</ion-label>
    <ion-chip color="primary">{{ doc.royalties }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherIncome">
    <ion-label>Other income</ion-label>
    <ion-chip color="primary">{{ doc.otherIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fishingBoatProceeds">
    <ion-label>Fishing boat proceeds</ion-label>
    <ion-chip color="primary">{{ doc.fishingBoatProceeds }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.medicalHealthPayment">
    <ion-label>Medical and health care payments</ion-label>
    <ion-chip color="primary">{{ doc.medicalHealthPayment }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.nonEmployeeCompensation">
    <ion-label>Nonemployee compensation</ion-label>
    <ion-chip color="primary">{{ doc.nonEmployeeCompensation }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.substitutePayments">
    <ion-label>Substitute payments in lieu of dividends or interest</ion-label>
    <ion-chip color="primary">{{ doc.substitutePayments }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.payerDirectSales">
    <ion-label>Payer made direct sales of $5,000 or more</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.payerDirectSales ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.cropInsurance">
    <ion-label>Crop insurance proceeds</ion-label>
    <ion-chip color="primary">{{ doc.cropInsurance }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.excessGolden">
    <ion-label>Excess golden parachute payments</ion-label>
    <ion-chip color="primary">{{ doc.excessGolden }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grossAttorney">
    <ion-label>Gross proceeds paid to an attorney</ion-label>
    <ion-chip color="primary">{{ doc.grossAttorney }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.section409ADeferrals">
    <ion-label>Section 409A deferrals</ion-label>
    <ion-chip color="primary">{{ doc.section409ADeferrals }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.section409AIncome">
    <ion-label>Section 409A income</ion-label>
    <ion-chip color="primary">{{ doc.section409AIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099MiscComponent implements OnInit {

    @Input()
    public doc: Tax1099Misc;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
