import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099B } from '../../../models/tax1099B';


@Component({
    selector: 'app-tax1099b',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>Payer's federal identification number</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>Recipient's identification number</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.profitOnClosedContracts">
    <ion-label>Profit or (loss) realized in 2019 on closed contracts</ion-label>
    <ion-chip color="primary">{{ doc.profitOnClosedContracts }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unrealizedProfitOpenContractsBegin">
    <ion-label>Unrealized profit or loss on open contracts - 12/31/2018</ion-label>
    <ion-chip color="primary">{{ doc.unrealizedProfitOpenContractsBegin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unrealizedProfitOpenContractsEnd">
    <ion-label>Unrealized profit or loss on open contracts - 12/31/2019</ion-label>
    <ion-chip color="primary">{{ doc.unrealizedProfitOpenContractsEnd }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.aggregateProfitOnContracts">
    <ion-label>Aggregate profit or (loss) on contracts</ion-label>
    <ion-chip color="primary">{{ doc.aggregateProfitOnContracts }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bartering">
    <ion-label>Bartering</ion-label>
    <ion-chip color="primary">{{ doc.bartering }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.securityDetails">
    <ion-label>Security details</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'SecurityDetail', doc.securityDetails ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099BComponent implements OnInit {

    @Input()
    public doc: Tax1099B;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
