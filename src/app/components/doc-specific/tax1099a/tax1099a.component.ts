import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099A } from '../../../models/tax1099A';


@Component({
    selector: 'app-tax1099a',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.lenderNameAddress">
    <ion-label>Lender's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.lenderNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.lenderTin">
    <ion-label>LENDER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.lenderTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.borrowerTin">
    <ion-label>BORROWER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.borrowerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.borrowerNameAddress">
    <ion-label>Borrower's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.borrowerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateOfAcquisition">
    <ion-label>Date of lender's acquisition or knowledge of abandonment</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateOfAcquisition ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.principalBalance">
    <ion-label>Balance of principal outstanding</ion-label>
    <ion-chip color="primary">{{ doc.principalBalance }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fairMarketValue">
    <ion-label>Fair market value property</ion-label>
    <ion-chip color="primary">{{ doc.fairMarketValue }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.personallyLiable">
    <ion-label>If checked, the borrower was personally liable for repayment of the debt</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.personallyLiable ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.propertyDescription">
    <ion-label>Description of property</ion-label>
    <ion-chip color="primary">{{ doc.propertyDescription }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099AComponent implements OnInit {

    @Input()
    public doc: Tax1099A;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
