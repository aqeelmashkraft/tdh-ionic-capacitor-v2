import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1095A } from '../../../models/tax1095A';


@Component({
    selector: 'app-tax1095a',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.marketplaceId">
    <ion-label>Marketplace identifier</ion-label>
    <ion-chip color="primary">{{ doc.marketplaceId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.marketplacePolicyNumber">
    <ion-label>Marketplace-assigned policy number</ion-label>
    <ion-chip color="primary">{{ doc.marketplacePolicyNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.policyIssuerName">
    <ion-label>Policy issuer's name</ion-label>
    <ion-chip color="primary">{{ doc.policyIssuerName }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientName">
    <ion-label>Recipient's name</ion-label>
    <ion-chip color="primary">{{ doc.recipientName }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>Recipient's SSN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientDateOfBirth">
    <ion-label>Recipient's date of birth</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.recipientDateOfBirth ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.spouseName">
    <ion-label>Recipient's spouse's name</ion-label>
    <ion-chip color="primary">{{ doc.spouseName }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.spouseTin">
    <ion-label>Recipient's spouse's SSN</ion-label>
    <ion-chip color="primary">{{ doc.spouseTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.spouseDateOfBirth">
    <ion-label>Recipient's spouse's date of birth</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.spouseDateOfBirth ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.policyStartDate">
    <ion-label>Policy start date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.policyStartDate ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.policyTerminationDate">
    <ion-label>Policy termination date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.policyTerminationDate ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientAddress">
    <ion-label>Recipient address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'Address', doc.recipientAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.coveredIndividuals">
    <ion-label>Covered Individuals</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'HealthInsuranceMarketplaceCoveredIndividual', doc.coveredIndividuals ) }}</pre></ion-item>

<ion-item *ngIf="doc.coverages">
    <ion-label>Coverage Information</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'HealthInsuranceCoverage', doc.coverages ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1095AComponent implements OnInit {

    @Input()
    public doc: Tax1095A;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
