import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099G } from '../../../models/tax1099G';


@Component({
    selector: 'app-tax1099g',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>PAYER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>RECIPIENT'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unemploymentCompensation">
    <ion-label>Unemployment compensation</ion-label>
    <ion-chip color="primary">{{ doc.unemploymentCompensation }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxRefund">
    <ion-label>State or local income tax refunds, credits, or offsets</ion-label>
    <ion-chip color="primary">{{ doc.taxRefund }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.refundYear">
    <ion-label>Box 2 amount is for tax year</ion-label>
    <ion-chip color="primary">{{ doc.refundYear }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.rtaaPayments">
    <ion-label>RTAA payments</ion-label>
    <ion-chip color="primary">{{ doc.rtaaPayments }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grants">
    <ion-label>Taxable grants</ion-label>
    <ion-chip color="primary">{{ doc.grants }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.agriculturePayments">
    <ion-label>Agriculture payments</ion-label>
    <ion-chip color="primary">{{ doc.agriculturePayments }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.businessIncome">
    <ion-label>If checked, box 2 is trade or business income</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.businessIncome ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.marketGain">
    <ion-label>Market gain</ion-label>
    <ion-chip color="primary">{{ doc.marketGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099GComponent implements OnInit {

    @Input()
    public doc: Tax1099G;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
