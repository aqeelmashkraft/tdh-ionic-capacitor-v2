import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1098 } from '../../../models/tax1098';


@Component({
    selector: 'app-tax1098',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.lenderNameAddress">
    <ion-label>Lender's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.lenderNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.lenderTin">
    <ion-label>RECIPIENT'S/LENDER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.lenderTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.borrowerTin">
    <ion-label>PAYER'S/BORROWER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.borrowerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.borrowerNameAddress">
    <ion-label>Borrower's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.borrowerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.mortgagedProperties">
    <ion-label>Number of properties securing the mortgage</ion-label>
    <ion-chip color="primary">{{ doc.mortgagedProperties }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.mortgageInterest">
    <ion-label>Mortgage interest received from borrower</ion-label>
    <ion-chip color="primary">{{ doc.mortgageInterest }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.outstandingPrincipal">
    <ion-label>Outstanding mortgage principal</ion-label>
    <ion-chip color="primary">{{ doc.outstandingPrincipal }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.originationDate">
    <ion-label>Mortgage origination date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.originationDate ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.overpaidRefund">
    <ion-label>Refund of overpaid interest</ion-label>
    <ion-chip color="primary">{{ doc.overpaidRefund }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.mortgageInsurance">
    <ion-label>Mortgage insurance premiums</ion-label>
    <ion-chip color="primary">{{ doc.mortgageInsurance }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.pointsPaid">
    <ion-label>Points paid on purchase of principal residence</ion-label>
    <ion-chip color="primary">{{ doc.pointsPaid }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.isPropertyAddressSameAsBorrowerAddress">
    <ion-label>Is address of property securing mortgage same as PAYER'S/BORROWER'S address</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.isPropertyAddressSameAsBorrowerAddress ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.acquisitionDate">
    <ion-label>Mortgage acquisition date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.acquisitionDate ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.propertyAddress">
    <ion-label>Address of property securing mortgage</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'Address', doc.propertyAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.propertyTax">
    <ion-label>Property tax</ion-label>
    <ion-chip color="primary">{{ doc.propertyTax }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherInformation">
    <ion-label>Other information</ion-label>
    <ion-chip color="primary">{{ doc.otherInformation }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.propertyDescription">
    <ion-label>Description of property securing mortgage, if property securing mortgage has no address</ion-label>
    <ion-chip color="primary">{{ doc.propertyDescription }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1098Component implements OnInit {

    @Input()
    public doc: Tax1098;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
