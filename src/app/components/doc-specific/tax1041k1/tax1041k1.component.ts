import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1041K1 } from '../../../models/tax1041K1';


@Component({
    selector: 'app-tax1041k1',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.finalK1">
    <ion-label>Final K-1</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.finalK1 ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.amendedK1">
    <ion-label>Amended K-1</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.amendedK1 ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fiscalYearBegin">
    <ion-label>Fiscal year begin date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearBegin ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fiscalYearEnd">
    <ion-label>Fiscal year end date</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.fiscalYearEnd ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.trustTin">
    <ion-label>Estate's or trust's employer identification number</ion-label>
    <ion-chip color="primary">{{ doc.trustTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.trustName">
    <ion-label>Estate's or trust's name</ion-label>
    <ion-chip color="primary">{{ doc.trustName }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.final1041T">
    <ion-label>Check if Form 1041-T was filed</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.final1041T ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.date1041T">
    <ion-label>and enter the date it was filed</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.date1041T ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.final1041">
    <ion-label>Check if this is the final Form 1041 for the estate or trust</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.final1041 ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.beneficiaryTin">
    <ion-label>Beneficiary's identifying number</ion-label>
    <ion-chip color="primary">{{ doc.beneficiaryTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.domestic">
    <ion-label>Domestic beneficiary</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.domestic ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreign">
    <ion-label>Foreign beneficiary</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreign ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.interestIncome">
    <ion-label>Interest income</ion-label>
    <ion-chip color="primary">{{ doc.interestIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ordinaryDividends">
    <ion-label>Ordinary dividends</ion-label>
    <ion-chip color="primary">{{ doc.ordinaryDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedDividends">
    <ion-label>Qualified dividends</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netShortTermGain">
    <ion-label>Net short-term capital gain</ion-label>
    <ion-chip color="primary">{{ doc.netShortTermGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netLongTermGain">
    <ion-label>Net long-term capital gain</ion-label>
    <ion-chip color="primary">{{ doc.netLongTermGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.gain28Rate">
    <ion-label>28% rate gain</ion-label>
    <ion-chip color="primary">{{ doc.gain28Rate }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unrecaptured1250Gain">
    <ion-label>Unrecaptured section 1250 gain</ion-label>
    <ion-chip color="primary">{{ doc.unrecaptured1250Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherPortfolioIncome">
    <ion-label>Other portfolio and nonbusiness income</ion-label>
    <ion-chip color="primary">{{ doc.otherPortfolioIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ordinaryBusinessIncome">
    <ion-label>Ordinary business income</ion-label>
    <ion-chip color="primary">{{ doc.ordinaryBusinessIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netRentalRealEstateIncome">
    <ion-label>Net rental real estate income</ion-label>
    <ion-chip color="primary">{{ doc.netRentalRealEstateIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherRentalIncome">
    <ion-label>Other rental income</ion-label>
    <ion-chip color="primary">{{ doc.otherRentalIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.directlyApportionedDeductions">
    <ion-label>Directly apportioned deductions</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.directlyApportionedDeductions">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.estateTaxDeduction">
    <ion-label>Estate tax deduction</ion-label>
    <ion-chip color="primary">{{ doc.estateTaxDeduction }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.finalYearDeductions">
    <ion-label>Final year deductions</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.finalYearDeductions">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.fiduciaryNameAddress">
    <ion-label>Fiduciary's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.fiduciaryNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.amtAdjustments">
    <ion-label>Alternative minimum tax adjustment</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.amtAdjustments">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.beneficiaryNameAddress">
    <ion-label>Beneficiary's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.beneficiaryNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.credits">
    <ion-label>Credits and credit recapture</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.credits">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>

<ion-item *ngIf="doc.otherInfo">
    <ion-label>Other information</ion-label>
</ion-item>
<ion-item *ngFor="let o of doc.otherInfo">
    <ion-chip slot="end" color="primary"> {{ o.code }}  {{ o.amount }} </ion-chip> 
</ion-item>


</ion-list>

    `
})
export class Tax1041K1Component implements OnInit {

    @Input()
    public doc: Tax1041K1;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
