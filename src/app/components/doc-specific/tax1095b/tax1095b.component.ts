import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1095B } from '../../../models/tax1095B';


@Component({
    selector: 'app-tax1095b',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.responsibleName">
    <ion-label>Name of responsible individual</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'IndividualName', doc.responsibleName ) }}</pre></ion-item>

<ion-item *ngIf="doc.responsibleTin">
    <ion-label>Social security number (SSN or other TIN)</ion-label>
    <ion-chip color="primary">{{ doc.responsibleTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.responsibleDateOfBirth">
    <ion-label>Date of birth (if SSN or other TIN is not available)</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.responsibleDateOfBirth ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.responsibleAddress">
    <ion-label>Address of responsible individual</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'Address', doc.responsibleAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.originOfHealthCoverageCode">
    <ion-label>Enter letter identifying Origin of the Health Coverage</ion-label>
    <ion-chip color="primary">{{ doc.originOfHealthCoverageCode }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.employerNameAddress">
    <ion-label>Employer name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.employerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.employerId">
    <ion-label>Employer identification number (EIN)</ion-label>
    <ion-chip color="primary">{{ doc.employerId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.issuerNameAddressPhone">
    <ion-label>Issuer name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.issuerNameAddressPhone ) }}</pre></ion-item>

<ion-item *ngIf="doc.issuerId">
    <ion-label>Employer identification number (EIN)</ion-label>
    <ion-chip color="primary">{{ doc.issuerId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.coveredIndividuals">
    <ion-label>Covered Individuals</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'HealthInsuranceCoveredIndividual', doc.coveredIndividuals ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1095BComponent implements OnInit {

    @Input()
    public doc: Tax1095B;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
