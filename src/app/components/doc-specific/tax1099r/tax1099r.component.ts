import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099R } from '../../../models/tax1099R';


@Component({
    selector: 'app-tax1099r',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>PAYER's TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>RECIPIENT's TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.allocableToIRR">
    <ion-label>Amount allocable to IRR within 5 years</ion-label>
    <ion-chip color="primary">{{ doc.allocableToIRR }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.firstYearOfRoth">
    <ion-label>First year of designated Roth</ion-label>
    <ion-chip color="primary">{{ doc.firstYearOfRoth }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignAccountTaxCompliance">
    <ion-label>FATCA filing requirement</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreignAccountTaxCompliance ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientAccountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.recipientAccountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateOfPayment">
    <ion-label>Date of payment</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateOfPayment ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grossDistribution">
    <ion-label>Gross distribution</ion-label>
    <ion-chip color="primary">{{ doc.grossDistribution }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxableAmount">
    <ion-label>Taxable amount</ion-label>
    <ion-chip color="primary">{{ doc.taxableAmount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxableAmountNotDetermined">
    <ion-label>Taxable amount not determined</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.taxableAmountNotDetermined ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.totalDistribution">
    <ion-label>Total distribution</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.totalDistribution ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.capitalGain">
    <ion-label>Capital gain</ion-label>
    <ion-chip color="primary">{{ doc.capitalGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.employeeContributions">
    <ion-label>Employee contributions</ion-label>
    <ion-chip color="primary">{{ doc.employeeContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.netUnrealizedAppreciation">
    <ion-label>Net unrealized appreciation</ion-label>
    <ion-chip color="primary">{{ doc.netUnrealizedAppreciation }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.distributionCodes">
    <ion-label>Distribution codes</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'array', doc.distributionCodes ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.iraSepSimple">
    <ion-label>IRA/SEP/SIMPLE</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.iraSepSimple ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherAmount">
    <ion-label>Other</ion-label>
    <ion-chip color="primary">{{ doc.otherAmount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherPercent">
    <ion-label>Other percent</ion-label>
    <ion-chip color="primary">{{ doc.otherPercent }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.yourPercentOfTotal">
    <ion-label>Your percent of total distribution</ion-label>
    <ion-chip color="primary">{{ doc.yourPercentOfTotal }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.totalEmployeeContributions">
    <ion-label>Total employee contributions</ion-label>
    <ion-chip color="primary">{{ doc.totalEmployeeContributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>

<ion-item *ngIf="doc.localTaxWithholding">
    <ion-label>Local tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'LocalTaxWithholding', doc.localTaxWithholding ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099RComponent implements OnInit {

    @Input()
    public doc: Tax1099R;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
