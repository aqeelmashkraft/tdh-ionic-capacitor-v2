import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1095C } from '../../../models/tax1095C';


@Component({
    selector: 'app-tax1095c',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.employeeName">
    <ion-label>Employee name</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'IndividualName', doc.employeeName ) }}</pre></ion-item>

<ion-item *ngIf="doc.tin">
    <ion-label>Social security number (SSN)</ion-label>
    <ion-chip color="primary">{{ doc.tin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.employeeAddress">
    <ion-label>Employee address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'Address', doc.employeeAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.employerNameAddressPhone">
    <ion-label>Employer name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.employerNameAddressPhone ) }}</pre></ion-item>

<ion-item *ngIf="doc.employerId">
    <ion-label>Employer identification number (EIN)</ion-label>
    <ion-chip color="primary">{{ doc.employerId }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.planStartMonth">
    <ion-label>Plan start month</ion-label>
    <ion-chip color="primary">{{ doc.planStartMonth }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.offersOfCoverage">
    <ion-label>Employee Offer of Coverage</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'OfferOfHealthInsuranceCoverage', doc.offersOfCoverage ) }}</pre></ion-item>

<ion-item *ngIf="doc.selfInsuredCoverage">
    <ion-label>Check if employer provided self-insured coverage</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.selfInsuredCoverage ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.coveredIndividuals">
    <ion-label>Covered Individuals</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'HealthInsuranceCoveredIndividual', doc.coveredIndividuals ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1095CComponent implements OnInit {

    @Input()
    public doc: Tax1095C;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
