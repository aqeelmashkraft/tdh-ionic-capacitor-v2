import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Patr } from '../../../models/tax1099Patr';


@Component({
    selector: 'app-tax1099patr',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>PAYER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>RECIPIENT'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.patronageDividends">
    <ion-label>Patronage dividends</ion-label>
    <ion-chip color="primary">{{ doc.patronageDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.nonpatronageDistributions">
    <ion-label>Nonpatronage distributions</ion-label>
    <ion-chip color="primary">{{ doc.nonpatronageDistributions }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.perUnitRetainAllocations">
    <ion-label>Per-unit retain allocations</ion-label>
    <ion-chip color="primary">{{ doc.perUnitRetainAllocations }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.redemption">
    <ion-label>Redemption of nonqualified notices and retain allocations</ion-label>
    <ion-chip color="primary">{{ doc.redemption }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dpaDeduction">
    <ion-label>Domestic production activities deduction</ion-label>
    <ion-chip color="primary">{{ doc.dpaDeduction }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedPayments">
    <ion-label>Qualified payments</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedPayments }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.investmentCredit">
    <ion-label>Investment credit</ion-label>
    <ion-chip color="primary">{{ doc.investmentCredit }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.workOpportunityCredit">
    <ion-label>Work opportunity credit</ion-label>
    <ion-chip color="primary">{{ doc.workOpportunityCredit }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.patronsAmtAdjustment">
    <ion-label>Patron's AMT adjustment</ion-label>
    <ion-chip color="primary">{{ doc.patronsAmtAdjustment }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.otherCreditsAndDeductions">
    <ion-label>Other credits and deductions</ion-label>
    <ion-chip color="primary">{{ doc.otherCreditsAndDeductions }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099PatrComponent implements OnInit {

    @Input()
    public doc: Tax1099Patr;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
