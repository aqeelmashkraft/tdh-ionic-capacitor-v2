import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099S } from '../../../models/tax1099S';


@Component({
    selector: 'app-tax1099s',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.filerNameAddress">
    <ion-label>Filer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.filerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.filerTin">
    <ion-label>FILER'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.filerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.transferorTin">
    <ion-label>TRANSFEROR'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.transferorTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.transferorNameAddress">
    <ion-label>Transferor's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.transferorNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account or escrow number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateOfClosing">
    <ion-label>Date of closing</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateOfClosing ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.grossProceeds">
    <ion-label>Gross proceeds</ion-label>
    <ion-chip color="primary">{{ doc.grossProceeds }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.addressOrLegalDescription">
    <ion-label>Address or legal description</ion-label>
    <ion-chip color="primary">{{ doc.addressOrLegalDescription }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.receivedOtherConsideration">
    <ion-label>Transferor received or will receive property or services as part of the consideration (if checked) .</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.receivedOtherConsideration ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignPerson">
    <ion-label>If checked, transferor is a foreign person (nonresident alien, Foreign partnership, foreign estate, or foreign trust)</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreignPerson ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.realEstateTax">
    <ion-label>Buyer's part of real estate tax</ion-label>
    <ion-chip color="primary">{{ doc.realEstateTax }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099SComponent implements OnInit {

    @Input()
    public doc: Tax1099S;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
