import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099C } from '../../../models/tax1099C';


@Component({
    selector: 'app-tax1099c',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.creditorNameAddress">
    <ion-label>Creditor's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.creditorNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.creditorTin">
    <ion-label>CREDITOR'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.creditorTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.debtorTin">
    <ion-label>DEBTOR'S TIN</ion-label>
    <ion-chip color="primary">{{ doc.debtorTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.debtorNameAddress">
    <ion-label>Debtor's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.debtorNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.dateOfEvent">
    <ion-label>Date of identifiable event</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Timestamp', doc.dateOfEvent ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.amountDischarged">
    <ion-label>Amount of debt discharged</ion-label>
    <ion-chip color="primary">{{ doc.amountDischarged }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.interestIncluded">
    <ion-label>Interest if included in box 2</ion-label>
    <ion-chip color="primary">{{ doc.interestIncluded }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.debtDescription">
    <ion-label>Debt description</ion-label>
    <ion-chip color="primary">{{ doc.debtDescription }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.personallyLiable">
    <ion-label>If checked, the debtor was personally liable for repayment of the debt</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.personallyLiable ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.debtCode">
    <ion-label>Identifiable debt code</ion-label>
    <ion-chip color="primary">{{ doc.debtCode }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.fairMarketValue">
    <ion-label>Fair market value of property</ion-label>
    <ion-chip color="primary">{{ doc.fairMarketValue }}</ion-chip>
</ion-item>


</ion-list>

    `
})
export class Tax1099CComponent implements OnInit {

    @Input()
    public doc: Tax1099C;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
