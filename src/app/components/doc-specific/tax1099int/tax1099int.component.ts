import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Int } from '../../../models/tax1099Int';


@Component({
    selector: 'app-tax1099int',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>Payer's TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>Recipient's TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.foreignAccountTaxCompliance">
    <ion-label>FATCA filing requirement</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreignAccountTaxCompliance ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.payerRtn">
    <ion-label>Payer's RTN</ion-label>
    <ion-chip color="primary">{{ doc.payerRtn }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.interestIncome">
    <ion-label>Interest income</ion-label>
    <ion-chip color="primary">{{ doc.interestIncome }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.earlyWithdrawalPenalty">
    <ion-label>Early withdrawal penalty</ion-label>
    <ion-chip color="primary">{{ doc.earlyWithdrawalPenalty }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.usBondInterest">
    <ion-label>Interest on U.S. Savings Bonds and Treasury obligations</ion-label>
    <ion-chip color="primary">{{ doc.usBondInterest }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.investmentExpenses">
    <ion-label>Investment expenses</ion-label>
    <ion-chip color="primary">{{ doc.investmentExpenses }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignTaxPaid">
    <ion-label>Foreign tax paid</ion-label>
    <ion-chip color="primary">{{ doc.foreignTaxPaid }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignCountry">
    <ion-label>Foreign country or U.S. possession</ion-label>
    <ion-chip color="primary">{{ doc.foreignCountry }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxExemptInterest">
    <ion-label>Tax-exempt interest</ion-label>
    <ion-chip color="primary">{{ doc.taxExemptInterest }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.specifiedPabInterest">
    <ion-label>Specified private activity bond interest</ion-label>
    <ion-chip color="primary">{{ doc.specifiedPabInterest }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.marketDiscount">
    <ion-label>Market discount</ion-label>
    <ion-chip color="primary">{{ doc.marketDiscount }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.bondPremium">
    <ion-label>Bond premium</ion-label>
    <ion-chip color="primary">{{ doc.bondPremium }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.usBondPremium">
    <ion-label>Bond premium on Treasury obligations</ion-label>
    <ion-chip color="primary">{{ doc.usBondPremium }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxExemptBondPremium">
    <ion-label>Bond premium on tax-exempt bond</ion-label>
    <ion-chip color="primary">{{ doc.taxExemptBondPremium }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.cusipNumber">
    <ion-label>Tax-exempt bond CUSIP no.</ion-label>
    <ion-chip color="primary">{{ doc.cusipNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099IntComponent implements OnInit {

    @Input()
    public doc: Tax1099Int;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
