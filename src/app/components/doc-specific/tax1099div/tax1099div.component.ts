import { Component, Input, OnInit } from '@angular/core';
import { FormatterService } from '../../../services/formatter-service';
import { Tax1099Div } from '../../../models/tax1099Div';


@Component({
    selector: 'app-tax1099div',
    template: `

<ion-list lines="none">

<ion-item *ngIf="doc.payerNameAddress">
    <ion-label>Payer's name, address, and phone</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddressPhone', doc.payerNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.payerTin">
    <ion-label>Payer's TIN</ion-label>
    <ion-chip color="primary">{{ doc.payerTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientTin">
    <ion-label>Recipient's TIN</ion-label>
    <ion-chip color="primary">{{ doc.recipientTin }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.recipientNameAddress">
    <ion-label>Recipient's name and address</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'NameAddress', doc.recipientNameAddress ) }}</pre></ion-item>

<ion-item *ngIf="doc.foreignAccountTaxCompliance">
    <ion-label>FATCA filing requirement</ion-label>
    <ion-chip color="primary">{{ formatter.format( 'Boolean', doc.foreignAccountTaxCompliance ) }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.accountNumber">
    <ion-label>Account number</ion-label>
    <ion-chip color="primary">{{ doc.accountNumber }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.ordinaryDividends">
    <ion-label>Total ordinary dividends</ion-label>
    <ion-chip color="primary">{{ doc.ordinaryDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.qualifiedDividends">
    <ion-label>Qualified dividends</ion-label>
    <ion-chip color="primary">{{ doc.qualifiedDividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.totalCapitalGain">
    <ion-label>Total capital gain distributions</ion-label>
    <ion-chip color="primary">{{ doc.totalCapitalGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.unrecaptured1250Gain">
    <ion-label>Unrecaptured Section 1250 gain</ion-label>
    <ion-chip color="primary">{{ doc.unrecaptured1250Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.section1202Gain">
    <ion-label>Section 1202 gain</ion-label>
    <ion-chip color="primary">{{ doc.section1202Gain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.collectiblesGain">
    <ion-label>Collectibles (28%) gain</ion-label>
    <ion-chip color="primary">{{ doc.collectiblesGain }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.nonTaxableDistribution">
    <ion-label>Nondividend distributions</ion-label>
    <ion-chip color="primary">{{ doc.nonTaxableDistribution }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.federalTaxWithheld">
    <ion-label>Federal income tax withheld</ion-label>
    <ion-chip color="primary">{{ doc.federalTaxWithheld }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.section199ADividends">
    <ion-label>Section 199A dividends</ion-label>
    <ion-chip color="primary">{{ doc.section199ADividends }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.investmentExpenses">
    <ion-label>Investment expenses</ion-label>
    <ion-chip color="primary">{{ doc.investmentExpenses }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignTaxPaid">
    <ion-label>Foreign tax paid</ion-label>
    <ion-chip color="primary">{{ doc.foreignTaxPaid }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.foreignCountry">
    <ion-label>Foreign country or U.S. possession</ion-label>
    <ion-chip color="primary">{{ doc.foreignCountry }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.cashLiquidation">
    <ion-label>Cash liquidation distributions</ion-label>
    <ion-chip color="primary">{{ doc.cashLiquidation }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.nonCashLiquidation">
    <ion-label>Noncash liquidation distributions</ion-label>
    <ion-chip color="primary">{{ doc.nonCashLiquidation }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.taxExemptInterestDividend">
    <ion-label>Exempt-interest dividends</ion-label>
    <ion-chip color="primary">{{ doc.taxExemptInterestDividend }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.specifiedPabInterestDividend">
    <ion-label>Specified private activity bond interest dividends</ion-label>
    <ion-chip color="primary">{{ doc.specifiedPabInterestDividend }}</ion-chip>
</ion-item>

<ion-item *ngIf="doc.stateTaxWithholding">
    <ion-label>State tax withholding</ion-label>
</ion-item>
<ion-item><pre>{{ formatter.format( 'StateTaxWithholding', doc.stateTaxWithholding ) }}</pre></ion-item>


</ion-list>

    `
})
export class Tax1099DivComponent implements OnInit {

    @Input()
    public doc: Tax1099Div;

    constructor(
        public formatter: FormatterService
    ) {

    }

    ngOnInit( ) {

    }

}
