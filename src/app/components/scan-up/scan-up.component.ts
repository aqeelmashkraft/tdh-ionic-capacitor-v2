import { Component, OnInit } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner/ngx';
import { Router } from '@angular/router';
import { GlobalDataService } from '../../services/global-data.service';
import {LoggerService} from '../../services/logger.service';

@Component( {
    selector: 'app-scan-up',
    templateUrl: './scan-up.component.html',
    styleUrls: [ './scan-up.component.scss' ],
} )
export class ScanUpComponent implements OnInit {

    options: BarcodeScannerOptions;

    results: BarcodeScanResult;

    constructor(
        private scanner: BarcodeScanner,
        private router: Router,
        private logger: LoggerService,
        private global: GlobalDataService
    ) {

    }

    ngOnInit( ) {

    }

    scanQrCode( ) {

        this.logger.trace( 'ScanUpComponent scanQrCode' );

        this.options = {
            prompt: 'Scan QR code',
            showTorchButton: true,
            showFlipCameraButton: false
        };

        this.scanner
            .scan( this.options )
            .then(
                ( barCodeData ) => {

                    if ( barCodeData.cancelled ) {

                        this.goToWelcome( );

                    } else {

                        this.logger.trace( 'scanQrCode completed' );

                        this.logger.dump( 'qrData', barCodeData.text );





                    }

                },
                ( error ) => {

                    this.logger.dump( 'scanner error', error );

                    this.goToWelcome( );

                }
            );

    }

    goToWelcome( ) {

        this.logger.trace( 'goToWelcome' );

        this.router.navigate( [ '/' ] );

    }

}
