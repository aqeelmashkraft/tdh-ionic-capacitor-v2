import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';
import { Router } from '@angular/router';
import {LoggerService} from '../../services/logger.service';

@Component({
  selector: 'app-view-scanned-submit',
  templateUrl: './view-scanned-submit.component.html',
  styleUrls: ['./view-scanned-submit.component.scss'],
})
export class ViewScannedSubmitComponent implements OnInit {

  public qrData: string;

  constructor(
      private global: GlobalDataService,
      private logger: LoggerService,
      private router: Router
  ) {

  }

  ngOnInit( ) {

    this.logger.trace( 'ViewScannedSubmitComponent ngOnInit' );

    this.qrData = this.global.qrData;

  }

  submitClicked( ) {

    this.logger.trace( 'ViewScannedSubmitComponent submitClicked' );

    this.logger.dump( 'qrData', this.qrData );

    // this.global.postSnappedData( this.qrData );

    this.router.navigate( [ '/', 'doc' ] );

  }

}
