import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';
import { Router } from '@angular/router';
import { TaxData } from '../../models/taxData';
import {LoggerService} from '../../services/logger.service';
import {TaxDocListItem} from '../../models/TaxDocListItem';

@Component( {
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: [ './upload.component.scss' ],
} )
export class UploadComponent implements OnInit {

    constructor(
        private global: GlobalDataService,
        private logger: LoggerService,
        private router: Router
    ) {

    }

    ngOnInit() {

    }

    onClickGoToScanUp( ) {

        this.logger.trace( 'SettingsComponent onClickLinkToLocker' );

        this.router.navigate( [ '/scan-up' ] );

    }

    simulateUpload( ) {

        this.logger.trace( 'SettingsComponent simulateUpload' );

        const taxData: TaxData = this.global.getRandomDoc( );

        const taxDocListItem: TaxDocListItem
            = this.global.convertTaxDataToListItem( taxData );

        // this.logger.dump( 'snappedData', taxData )

        this.global.postSnappedData( taxData, taxDocListItem );

    }

}
