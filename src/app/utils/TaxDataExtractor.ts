import { TaxData } from '../models/taxData';
import { NameAddress } from '../models/nameAddress';
import { Tax1041K1 } from '../models/tax1041K1';
import { Tax1065K1 } from '../models/tax1065K1';
import { Tax1095A } from '../models/tax1095A';
import { Tax1095B } from '../models/tax1095B';
import { Tax1095C } from '../models/tax1095C';
import { Tax1097Btc } from '../models/tax1097Btc';
import { Tax1098 } from '../models/tax1098';
import { Tax1098C } from '../models/tax1098C';
import { Tax1098E } from '../models/tax1098E';
import { Tax1098T } from '../models/tax1098T';
import { Tax1099A } from '../models/tax1099A';
import { Tax1099B } from '../models/tax1099B';
import { Tax1099C } from '../models/tax1099C';
import { Tax1099Cap } from '../models/tax1099Cap';
import { Tax1099Div } from '../models/tax1099Div';
import { Tax1099G } from '../models/tax1099G';
import { Tax1099H } from '../models/tax1099H';
import { Tax1099Int } from '../models/tax1099Int';
import { Tax1099K } from '../models/tax1099K';
import { Tax1099Ltc } from '../models/tax1099Ltc';
import { Tax1099Misc } from '../models/tax1099Misc';
import { Tax1099Oid } from '../models/tax1099Oid';
import { Tax1099Patr } from '../models/tax1099Patr';
import { Tax1099Q } from '../models/tax1099Q';
import { Tax1099R } from '../models/tax1099R';
import { Tax1099S } from '../models/tax1099S';
import { Tax1099Sa } from '../models/tax1099Sa';
import { Tax1120SK1 } from '../models/tax1120SK1';
import { Tax2439 } from '../models/tax2439';
import { Tax5498 } from '../models/tax5498';
import { Tax5498Esa } from '../models/tax5498Esa';
import { Tax5498Sa } from '../models/tax5498Sa';
import { TaxW2 } from '../models/taxW2';
import { TaxW2G } from '../models/taxW2G';

export class TaxDataExtractor {

    public docId: string = '0';

    public docType: string = '';

    public docNumber: string = '';

    public description: string = '';

    public imageUrl: string = '';

    public issuerName: string = '';

    constructor(  ) {

    }

    extractName( nameAddress: NameAddress ): string {

        if ( nameAddress != null ) {
            return nameAddress.name1;
        }

        return '';

    }

    extractForTaxData(
        taxData: TaxData
    ) {

        if ( taxData.tax1041K1 != null ) {
            this.extractTax1041K1( taxData.tax1041K1 );
            return;
        }

        if ( taxData.tax1065K1 != null ) {
            this.extractTax1065K1( taxData.tax1065K1 );
            return;
        }

        if ( taxData.tax1095A != null ) {
            this.extractTax1095A( taxData.tax1095A );
            return;
        }

        if ( taxData.tax1095B != null ) {
            this.extractTax1095B( taxData.tax1095B );
            return;
        }

        if ( taxData.tax1095C != null ) {
            this.extractTax1095C( taxData.tax1095C );
            return;
        }

        if ( taxData.tax1097Btc != null ) {
            this.extractTax1097Btc( taxData.tax1097Btc );
            return;
        }

        if ( taxData.tax1098 != null ) {
            this.extractTax1098( taxData.tax1098 );
            return;
        }

        if ( taxData.tax1098C != null ) {
            this.extractTax1098C( taxData.tax1098C );
            return;
        }

        if ( taxData.tax1098E != null ) {
            this.extractTax1098E( taxData.tax1098E );
            return;
        }

        if ( taxData.tax1098T != null ) {
            this.extractTax1098T( taxData.tax1098T );
            return;
        }

        if ( taxData.tax1099A != null ) {
            this.extractTax1099A( taxData.tax1099A );
            return;
        }

        if ( taxData.tax1099B != null ) {
            this.extractTax1099B( taxData.tax1099B );
            return;
        }

        if ( taxData.tax1099C != null ) {
            this.extractTax1099C( taxData.tax1099C );
            return;
        }

        if ( taxData.tax1099Cap != null ) {
            this.extractTax1099Cap( taxData.tax1099Cap );
            return;
        }

        if ( taxData.tax1099Div != null ) {
            this.extractTax1099Div( taxData.tax1099Div );
            return;
        }

        if ( taxData.tax1099G != null ) {
            this.extractTax1099G( taxData.tax1099G );
            return;
        }

        if ( taxData.tax1099H != null ) {
            this.extractTax1099H( taxData.tax1099H );
            return;
        }

        if ( taxData.tax1099Int != null ) {
            this.extractTax1099Int( taxData.tax1099Int );
            return;
        }

        if ( taxData.tax1099K != null ) {
            this.extractTax1099K( taxData.tax1099K );
            return;
        }

        if ( taxData.tax1099Ltc != null ) {
            this.extractTax1099Ltc( taxData.tax1099Ltc );
            return;
        }

        if ( taxData.tax1099Misc != null ) {
            this.extractTax1099Misc( taxData.tax1099Misc );
            return;
        }

        if ( taxData.tax1099Oid != null ) {
            this.extractTax1099Oid( taxData.tax1099Oid );
            return;
        }

        if ( taxData.tax1099Patr != null ) {
            this.extractTax1099Patr( taxData.tax1099Patr );
            return;
        }

        if ( taxData.tax1099Q != null ) {
            this.extractTax1099Q( taxData.tax1099Q );
            return;
        }

        if ( taxData.tax1099R != null ) {
            this.extractTax1099R( taxData.tax1099R );
            return;
        }

        if ( taxData.tax1099S != null ) {
            this.extractTax1099S( taxData.tax1099S );
            return;
        }

        if ( taxData.tax1099Sa != null ) {
            this.extractTax1099Sa( taxData.tax1099Sa );
            return;
        }

        if ( taxData.tax1120SK1 != null ) {
            this.extractTax1120SK1( taxData.tax1120SK1 );
            return;
        }

        if ( taxData.tax2439 != null ) {
            this.extractTax2439( taxData.tax2439 );
            return;
        }

        if ( taxData.tax5498 != null ) {
            this.extractTax5498( taxData.tax5498 );
            return;
        }

        if ( taxData.tax5498Esa != null ) {
            this.extractTax5498Esa( taxData.tax5498Esa );
            return;
        }

        if ( taxData.tax5498Sa != null ) {
            this.extractTax5498Sa( taxData.tax5498Sa );
            return;
        }

        if ( taxData.taxW2 != null ) {
            this.extractTaxW2( taxData.taxW2 );
            return;
        }

        if ( taxData.taxW2G != null ) {
            this.extractTaxW2G( taxData.taxW2G );
            return;
        }

    }

    extractTax1041K1(
        form: Tax1041K1
    ) {

        this.docType = 'Tax1041K1';

        this.docNumber = '1041-K-1';

        this.description = "Beneficiary's Share of Income, Deductions, Credits, etc.";

        this.imageUrl = '/assets/img/f1041sk1.recipient.png';

        this.issuerName = this.extractName( form.fiduciaryNameAddress );

    }

    extractTax1065K1(
        form: Tax1065K1
    ) {

        this.docType = 'Tax1065K1';

        this.docNumber = '1065-K-1';

        this.description = "Partner's Share of Income, Deductions, Credits, etc.";

        this.imageUrl = '/assets/img/f1065sk1.recipient.png';

        this.issuerName = this.extractName( form.partnershipNameAddress );

    }

    extractTax1095A(
        form: Tax1095A
    ) {

        this.docType = 'Tax1095A';

        this.docNumber = '1095-A';

        this.description = "Health Insurance Marketplace Statement";

        this.imageUrl = '/assets/img/f1095a.recipient.png';


    }

    extractTax1095B(
        form: Tax1095B
    ) {

        this.docType = 'Tax1095B';

        this.docNumber = '1095-B';

        this.description = "Health Coverage";

        this.imageUrl = '/assets/img/f1095b.recipient.png';


    }

    extractTax1095C(
        form: Tax1095C
    ) {

        this.docType = 'Tax1095C';

        this.docNumber = '1095-C';

        this.description = "Employer-Provided Health Insurance Offer and Coverage";

        this.imageUrl = '/assets/img/f1095c.recipient.png';


    }

    extractTax1097Btc(
        form: Tax1097Btc
    ) {

        this.docType = 'Tax1097Btc';

        this.docNumber = '1097-BTC';

        this.description = "Bond Tax Credit";

        this.imageUrl = '/assets/img/f1097btc.recipient.png';

        this.issuerName = this.extractName( form.issuerNameAddress );

    }

    extractTax1098(
        form: Tax1098
    ) {

        this.docType = 'Tax1098';

        this.docNumber = '1098';

        this.description = "Mortgage Interest Statement";

        this.imageUrl = '/assets/img/f1098.recipient.png';

        this.issuerName = this.extractName( form.lenderNameAddress );

    }

    extractTax1098C(
        form: Tax1098C
    ) {

        this.docType = 'Tax1098C';

        this.docNumber = '1098-C';

        this.description = "Contributions of Motor Vehicles, Boats, and Airplanes";

        this.imageUrl = '/assets/img/f1098c.recipient.png';

        this.issuerName = this.extractName( form.doneeNameAddress );

    }

    extractTax1098E(
        form: Tax1098E
    ) {

        this.docType = 'Tax1098E';

        this.docNumber = '1098-E';

        this.description = "Student Loan Interest Statement";

        this.imageUrl = '/assets/img/f1098e.recipient.png';

        this.issuerName = this.extractName( form.lenderNameAddress );

    }

    extractTax1098T(
        form: Tax1098T
    ) {

        this.docType = 'Tax1098T';

        this.docNumber = '1098-T';

        this.description = "Tuition Statement";

        this.imageUrl = '/assets/img/f1098t.recipient.png';

        this.issuerName = this.extractName( form.filerNameAddress );

    }

    extractTax1099A(
        form: Tax1099A
    ) {

        this.docType = 'Tax1099A';

        this.docNumber = '1099-A';

        this.description = "Acquisition or Abandonment of Secured Property";

        this.imageUrl = '/assets/img/f1099a.recipient.png';

        this.issuerName = this.extractName( form.lenderNameAddress );

    }

    extractTax1099B(
        form: Tax1099B
    ) {

        this.docType = 'Tax1099B';

        this.docNumber = '1099-B';

        this.description = "Proceeds From Broker and Barter Exchange Transactions";

        this.imageUrl = '/assets/img/f1099b.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099C(
        form: Tax1099C
    ) {

        this.docType = 'Tax1099C';

        this.docNumber = '1099-C';

        this.description = "Cancellation of Debt";

        this.imageUrl = '/assets/img/f1099c.recipient.png';

        this.issuerName = this.extractName( form.creditorNameAddress );

    }

    extractTax1099Cap(
        form: Tax1099Cap
    ) {

        this.docType = 'Tax1099Cap';

        this.docNumber = '1099-CAP';

        this.description = "Changes in Corporate Control and Capital Structure";

        this.imageUrl = '/assets/img/f1099cap.recipient.png';

        this.issuerName = this.extractName( form.corporationNameAddress );

    }

    extractTax1099Div(
        form: Tax1099Div
    ) {

        this.docType = 'Tax1099Div';

        this.docNumber = '1099-DIV';

        this.description = "Dividends and Distributions";

        this.imageUrl = '/assets/img/f1099div.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099G(
        form: Tax1099G
    ) {

        this.docType = 'Tax1099G';

        this.docNumber = '1099-G';

        this.description = "Certain Government Payments";

        this.imageUrl = '/assets/img/f1099g.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099H(
        form: Tax1099H
    ) {

        this.docType = 'Tax1099H';

        this.docNumber = '1099-H';

        this.description = "Health Coverage Tax Credit (HCTC) Advance Payments";

        this.imageUrl = '/assets/img/f1099h.recipient.png';

        this.issuerName = this.extractName( form.issuerNameAddress );

    }

    extractTax1099Int(
        form: Tax1099Int
    ) {

        this.docType = 'Tax1099Int';

        this.docNumber = '1099-INT';

        this.description = "Interest Income";

        this.imageUrl = '/assets/img/f1099int.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099K(
        form: Tax1099K
    ) {

        this.docType = 'Tax1099K';

        this.docNumber = '1099-K';

        this.description = "Merchant Card and Third-Party Network Payments";

        this.imageUrl = '/assets/img/f1099k.recipient.png';

        this.issuerName = this.extractName( form.filerNameAddress );

    }

    extractTax1099Ltc(
        form: Tax1099Ltc
    ) {

        this.docType = 'Tax1099Ltc';

        this.docNumber = '1099-LTC';

        this.description = "Long-Term Care and Accelerated Death Benefits";

        this.imageUrl = '/assets/img/f1099ltc.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099Misc(
        form: Tax1099Misc
    ) {

        this.docType = 'Tax1099Misc';

        this.docNumber = '1099-MISC';

        this.description = "Miscellaneous Income";

        this.imageUrl = '/assets/img/f1099msc.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099Oid(
        form: Tax1099Oid
    ) {

        this.docType = 'Tax1099Oid';

        this.docNumber = '1099-OID';

        this.description = "Original Issue Discount";

        this.imageUrl = '/assets/img/f1099oid.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099Patr(
        form: Tax1099Patr
    ) {

        this.docType = 'Tax1099Patr';

        this.docNumber = '1099-PATR';

        this.description = "Taxable Distributions Received From Cooperatives";

        this.imageUrl = '/assets/img/f1099ptr.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099Q(
        form: Tax1099Q
    ) {

        this.docType = 'Tax1099Q';

        this.docNumber = '1099-Q';

        this.description = "Payments From Qualified Education Programs";

        this.imageUrl = '/assets/img/f1099q.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099R(
        form: Tax1099R
    ) {

        this.docType = 'Tax1099R';

        this.docNumber = '1099-R';

        this.description = "Distributions from Pensions, Annuities, Retirement or Profit-Sharing Plans, IRAs, Insurance Contracts, etc.";

        this.imageUrl = '/assets/img/f1099r.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1099S(
        form: Tax1099S
    ) {

        this.docType = 'Tax1099S';

        this.docNumber = '1099-S';

        this.description = "Proceeds From Real Estate Transactions";

        this.imageUrl = '/assets/img/f1099s.recipient.png';

        this.issuerName = this.extractName( form.filerNameAddress );

    }

    extractTax1099Sa(
        form: Tax1099Sa
    ) {

        this.docType = 'Tax1099Sa';

        this.docNumber = '1099-SA';

        this.description = "Distributions From an HSA, Archer MSA, or Medicare Advantage MSA";

        this.imageUrl = '/assets/img/f1099sa.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }

    extractTax1120SK1(
        form: Tax1120SK1
    ) {

        this.docType = 'Tax1120SK1';

        this.docNumber = '1120S-K-1';

        this.description = "Shareholder's Share of Income, Deductions, Credits, etc.";

        this.imageUrl = '/assets/img/f1120ssk.recipient.png';

        this.issuerName = this.extractName( form.corporationNameAddress );

    }

    extractTax2439(
        form: Tax2439
    ) {

        this.docType = 'Tax2439';

        this.docNumber = '2439';

        this.description = "Notice to Shareholder of Undistributed Long-Term Capital Gains";

        this.imageUrl = '/assets/img/f2439.recipient.png';

        this.issuerName = this.extractName( form.ricOrReitNameAddress );

    }

    extractTax5498(
        form: Tax5498
    ) {

        this.docType = 'Tax5498';

        this.docNumber = '5498';

        this.description = "IRA Contribution Information";

        this.imageUrl = '/assets/img/f5498.recipient.png';

        this.issuerName = this.extractName( form.issuerNameAddress );

    }

    extractTax5498Esa(
        form: Tax5498Esa
    ) {

        this.docType = 'Tax5498Esa';

        this.docNumber = '5498-ESA';

        this.description = "Coverdell ESA Contribution Information";

        this.imageUrl = '/assets/img/f5498e.recipient.png';

        this.issuerName = this.extractName( form.issuerNameAddress );

    }

    extractTax5498Sa(
        form: Tax5498Sa
    ) {

        this.docType = 'Tax5498Sa';

        this.docNumber = '5498-SA';

        this.description = "HSA, Archer MSA, or Medicare Advantage MSA Information";

        this.imageUrl = '/assets/img/f5498sa.recipient.png';

        this.issuerName = this.extractName( form.trusteeNameAddress );

    }

    extractTaxW2(
        form: TaxW2
    ) {

        this.docType = 'TaxW2';

        this.docNumber = 'W-2';

        this.description = "Wage and Tax Statement";

        this.imageUrl = '/assets/img/fw2.recipient.png';

        this.issuerName = this.extractName( form.employerNameAddress );

    }

    extractTaxW2G(
        form: TaxW2G
    ) {

        this.docType = 'TaxW2G';

        this.docNumber = 'W-2G';

        this.description = "Certain Gambling Winnings";

        this.imageUrl = '/assets/img/fw2g.recipient.png';

        this.issuerName = this.extractName( form.payerNameAddress );

    }


}

