// ===========================================================================================
// Angular
// ===========================================================================================
import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

// ===========================================================================================
// Capacitor
// ===========================================================================================
import { Plugins, Capacitor } from '@capacitor/core';

// ===========================================================================================
// Rxjs
// ===========================================================================================
import { BehaviorSubject, Observable, Subject } from 'rxjs';

// ===========================================================================================
// Third party
// ===========================================================================================
import { sha256 } from 'js-sha256';
import { UUID } from 'angular2-uuid';

// ===========================================================================================
// App
// ===========================================================================================
import { TaxDocListItem } from '../models/TaxDocListItem';
import { DescriptionValue } from '../models/descriptionValue';
import { TaxData } from '../models/taxData';
import { TaxDataExtractor } from '../utils/TaxDataExtractor';
import { LocalStorageService } from "./local-storage-service";
import { LoggerService } from "./logger.service";
import { TaxDataForQR } from '../models/taxDataForQR';




@Injectable( {
    providedIn: 'root'
} )
export class GlobalDataService {

    private apiRoot: string
        = 'https://taxdochub-endpoints.appspot.com/_ah/api/taxdochubapi/v1';

    private servletRoot: string
        = 'https://taxdochub-endpoints.appspot.com';

    private apiKey: string
        = 'AIzaSyC7lXKPr4PlPAb3f3MMqG2KeW7lIqOGrf0';

    public testMode: boolean = false;

    public showClear: boolean = false;

    // ==============================================================
    // Constants Data
    // ==============================================================
    public SCAN_DUPLICATE: number = -1;
    public SCAN_ERROR: number = -2;
    public PARSE_ERROR: number = -3;
    public PARSE_OK: number = 99;

    // ==============================================================
    // Device Data
    // ==============================================================
    public deviceGuid$: Observable<string>
        = this.storage.deviceGuidSubject.asObservable( );

    public lockerNumber$: Observable<string>
        = this.storage.deviceLockerSubject.asObservable( );

    // ==============================================================
    // Tax Documents
    // ==============================================================
    public taxDocuments$: Observable<TaxDocListItem[]>
        = this.storage.taxDocListSubject.asObservable();

    // public taxDocs: TaxDocListItem[] = []; // this.storage.taxDocs;

    // ==============================================================
    // User Data
    // ==============================================================
    public autoSync: boolean = false;

    // Data embedded in snapped QR code
    private _qrData: string;

    // ???
    public lastScannedText: string;

    // ???
    public deviceInitializeDateAsLong: number;

    // ???
    public serverResponse: string;

    // ???
    public metadataList: any[];


    // ==============================================================
    // Upload scanned data response
    // ==============================================================
    private uploadDataSubject = new BehaviorSubject<any>({});
    public uploadDataResponse$: Observable<any>
        = this.uploadDataSubject.asObservable();

    // ==============================================================
    // List of docs shown on home page
    // ==============================================================
    public docs: any[] = [
        {
            number: 'W-2',
            title: 'Wages'
        },
        {
            number: '1099-INT',
            title: 'Interest Income'
        },
        {
            number: '1098',
            title: 'Mortgage Interest'
        },
    ];

    // ==============================================================
    // List of programs shown on home page
    // ==============================================================
    public programs: any[] = [
        {
            name: 'H&R Block',
        },
        {
            name: 'Intuit TurboTax',
        },
        {
            name: 'TaxAct',
        },
    ];

    constructor(
        private httpClient: HttpClient,
        public storage: LocalStorageService,
        private logger: LoggerService
    ) {

        // this.taxDocs = this.storage.taxDocs;

        // Does not work as expected 2019 10 11 BVW
        // if ( isDevMode() ) {
        //     this.testMode = true;
        // }

    }

    set qrData( newQrData: string ) {
        this._qrData = newQrData;
    }

    get qrData( ): string {
        return this._qrData;
    }

    public turnOffTestMode( ) {

        this.testMode = false;

    }

    public toggleTestMode( ) {

        this.testMode = ! this.testMode;

    }

    public getTaxDocs( ): TaxDocListItem[] {

        return this.storage.taxDocs;

    }

    public getNumTaxDocs( ): number {

        return this.storage.taxDocs.length;

    }

    public getDeviceGuid( ): string {

        return this.storage.deviceGuid;
    }

    public clearTaxDocs( ): void {

        this.storage.clearTaxDataStorage( );

    }

    init( ) {

        this.logger.trace( `GlobalDataService init` );

        this.storage.initializeStorage( );

        // this.deviceGuid$ = this.getDeviceGuid( );

        // this.clearTaxDataStorage( );

        // Get data from storage
        // this.retrieveTaxDataFromStorage( )

        // const taxDataTax1041K1: TaxData = require( '../data/Tax1041K1.json' );
        // this.addTaxDoc( taxDataTax1041K1 );

        // const taxDataTax1041K1b: TaxData = require( '../data/Tax1041K1.json' );
        // this.addTaxDoc( taxDataTax1041K1b );

        this.loadSampleDocs( 0 );

    }

    randomInt(
        min: number,
        max: number
    ): number {

        return Math.floor( Math.random( ) * ( max - min + 1 ) ) + min;

    }

    getRandomDocJson( ): string  {

        const taxData: TaxData = this.getRandomDoc( );

        const asJson: string = JSON.stringify( taxData, null, 2 );

        return asJson;

    }

    getRandomDoc( ): TaxData  {

        const randomNumber: number = this.randomInt( 1, 34 )

        const taxData: TaxData = this.getRandomDocGivenNumber( randomNumber );

        return taxData;

    }

    getRandomDocGivenNumber(
        randomNumber: number
    ): TaxData {

        let taxData: TaxData = null;

        switch ( randomNumber ) {

            case 1:
                taxData = require( '../data/Tax1041K1.json' );
                break;

            case 2:
                taxData = require( '../data/Tax1065K1.json' );
                break;

            case 3:
                taxData = require( '../data/Tax1095A.json' );
                break;

            case 4:
                taxData = require( '../data/Tax1095B.json' );
                break;

            case 5:
                taxData = require( '../data/Tax1095C.json' );
                break;

            case 6:
                taxData = require( '../data/Tax1097Btc.json' );
                break;

            case 7:
                taxData = require( '../data/Tax1098.json' );
                break;

            case 8:
                taxData = require( '../data/Tax1098C.json' );
                break;

            case 9:
                taxData = require( '../data/Tax1098E.json' );
                break;

            case 10:
                taxData = require( '../data/Tax1098T.json' );
                break;

            case 11:
                taxData = require( '../data/Tax1099A.json' );
                break;

            case 12:
                taxData = require( '../data/Tax1099B.json' );
                break;

            case 13:
                taxData = require( '../data/Tax1099C.json' );
                break;

            case 14:
                taxData = require( '../data/Tax1099Cap.json' );
                break;

            case 15:
                taxData = require( '../data/Tax1099Div.json' );
                break;

            case 16:
                taxData = require( '../data/Tax1099G.json' );
                break;

            case 17:
                taxData = require( '../data/Tax1099H.json' );
                break;

            case 18:
                taxData = require( '../data/Tax1099Int.json' );
                break;

            case 19:
                taxData = require( '../data/Tax1099K.json' );
                break;

            case 20:
                taxData = require( '../data/Tax1099Ltc.json' );
                break;

            case 21:
                taxData = require( '../data/Tax1099Misc.json' );
                break;

            case 22:
                taxData = require( '../data/Tax1099Oid.json' );
                break;

            case 23:
                taxData = require( '../data/Tax1099Patr.json' );
                break;

            case 24:
                taxData = require( '../data/Tax1099Q.json' );
                break;

            case 25:
                taxData = require( '../data/Tax1099R.json' );
                break;

            case 26:
                taxData = require( '../data/Tax1099S.json' );
                break;

            case 27:
                taxData = require( '../data/Tax1099Sa.json' );
                break;

            case 28:
                taxData = require( '../data/Tax1120SK1.json' );
                break;

            case 29:
                taxData = require( '../data/Tax2439.json' );
                break;

            case 30:
                taxData = require( '../data/Tax5498.json' );
                break;

            case 31:
                taxData = require( '../data/Tax5498Esa.json' );
                break;

            case 32:
                taxData = require( '../data/Tax5498Sa.json' );
                break;

            case 33:
                taxData = require( '../data/TaxW2.json' );
                break;

            case 34:
                taxData = require( '../data/TaxW2G.json' );
                break;

            default:
                taxData = require( '../data/TaxW2.json' );

        }

        return taxData;

    }

    loadSampleDocs(
        quantity: number
    ) {

        if ( quantity < 1 ) return;

        const taxDataTaxW2: TaxData = require( '../data/TaxW2.json' );
        this.addTaxDoc( taxDataTaxW2 );

        if ( quantity < 2 ) return;

        const taxDataTax1041K1: TaxData = require( '../data/Tax1041K1.json' );
        this.addTaxDoc( taxDataTax1041K1 );

        if ( quantity < 3 ) return;

        const taxDataTax1065K1: TaxData = require( '../data/Tax1065K1.json' );
        this.addTaxDoc( taxDataTax1065K1 );

        if ( quantity < 4 ) return;

        const taxDataTax1095A: TaxData = require( '../data/Tax1095A.json' );
        this.addTaxDoc( taxDataTax1095A );

        if ( quantity < 4 ) return;

        const taxDataTax1095B: TaxData = require( '../data/Tax1095B.json' );
        this.addTaxDoc( taxDataTax1095B );

        if ( quantity < 5 ) return;

        const taxDataTax1095C: TaxData = require( '../data/Tax1095C.json' );
        this.addTaxDoc( taxDataTax1095C );

        if ( quantity < 6 ) return;

        const taxDataTax1097Btc: TaxData = require( '../data/Tax1097Btc.json' );
        this.addTaxDoc( taxDataTax1097Btc );

        if ( quantity < 7 ) return;

        const taxDataTax1098: TaxData = require( '../data/Tax1098.json' );
        this.addTaxDoc( taxDataTax1098 );

        if ( quantity < 8 ) return;

        const taxDataTax1098C: TaxData = require( '../data/Tax1098C.json' );
        this.addTaxDoc( taxDataTax1098C );

        if ( quantity < 6 ) return;

        const taxDataTax1098E: TaxData = require( '../data/Tax1098E.json' );
        this.addTaxDoc( taxDataTax1098E );

        if ( quantity < 6 ) return;

        const taxDataTax1098T: TaxData = require( '../data/Tax1098T.json' );
        this.addTaxDoc( taxDataTax1098T );

        if ( quantity < 6 ) return;

        const taxDataTax1099A: TaxData = require( '../data/Tax1099A.json' );
        this.addTaxDoc( taxDataTax1099A );

        if ( quantity < 6 ) return;

        const taxDataTax1099B: TaxData = require( '../data/Tax1099B.json' );
        this.addTaxDoc( taxDataTax1099B );

        if ( quantity < 6 ) return;

        const taxDataTax1099C: TaxData = require( '../data/Tax1099C.json' );
        this.addTaxDoc( taxDataTax1099C );

        if ( quantity < 6 ) return;

        const taxDataTax1099Cap: TaxData = require( '../data/Tax1099Cap.json' );
        this.addTaxDoc( taxDataTax1099Cap );

        if ( quantity < 6 ) return;

        const taxDataTax1099Div: TaxData = require( '../data/Tax1099Div.json' );
        this.addTaxDoc( taxDataTax1099Div );

        if ( quantity < 6 ) return;

        const taxDataTax1099G: TaxData = require( '../data/Tax1099G.json' );
        this.addTaxDoc( taxDataTax1099G );

        if ( quantity < 6 ) return;

        const taxDataTax1099H: TaxData = require( '../data/Tax1099H.json' );
        this.addTaxDoc( taxDataTax1099H );

        if ( quantity < 6 ) return;

        const taxDataTax1099Int: TaxData = require( '../data/Tax1099Int.json' );
        this.addTaxDoc( taxDataTax1099Int );

        if ( quantity < 6 ) return;

        const taxDataTax1099K: TaxData = require( '../data/Tax1099K.json' );
        this.addTaxDoc( taxDataTax1099K );

        if ( quantity < 6 ) return;

        const taxDataTax1099Ltc: TaxData = require( '../data/Tax1099Ltc.json' );
        this.addTaxDoc( taxDataTax1099Ltc );

        if ( quantity < 6 ) return;

        const taxDataTax1099Misc: TaxData = require( '../data/Tax1099Misc.json' );
        this.addTaxDoc( taxDataTax1099Misc );

        if ( quantity < 6 ) return;

        const taxDataTax1099Oid: TaxData = require( '../data/Tax1099Oid.json' );
        this.addTaxDoc( taxDataTax1099Oid );

        if ( quantity < 6 ) return;

        const taxDataTax1099Patr: TaxData = require( '../data/Tax1099Patr.json' );
        this.addTaxDoc( taxDataTax1099Patr );

        if ( quantity < 6 ) return;

        const taxDataTax1099Q: TaxData = require( '../data/Tax1099Q.json' );
        this.addTaxDoc( taxDataTax1099Q );

        if ( quantity < 6 ) return;

        const taxDataTax1099R: TaxData = require( '../data/Tax1099R.json' );
        this.addTaxDoc( taxDataTax1099R );

        if ( quantity < 6 ) return;

        const taxDataTax1099S: TaxData = require( '../data/Tax1099S.json' );
        this.addTaxDoc( taxDataTax1099S );

        if ( quantity < 6 ) return;

        const taxDataTax1099Sa: TaxData = require( '../data/Tax1099Sa.json' );
        this.addTaxDoc( taxDataTax1099Sa );

        if ( quantity < 6 ) return;

        const taxDataTax1120SK1: TaxData = require( '../data/Tax1120SK1.json' );
        this.addTaxDoc( taxDataTax1120SK1 );

        if ( quantity < 6 ) return;

        const taxDataTax2439: TaxData = require( '../data/Tax2439.json' );
        this.addTaxDoc( taxDataTax2439 );

        if ( quantity < 6 ) return;

        const taxDataTax5498: TaxData = require( '../data/Tax5498.json' );
        this.addTaxDoc( taxDataTax5498 );

        if ( quantity < 6 ) return;

        const taxDataTax5498Esa: TaxData = require( '../data/Tax5498Esa.json' );
        this.addTaxDoc( taxDataTax5498Esa );

        if ( quantity < 6 ) return;

        const taxDataTax5498Sa: TaxData = require( '../data/Tax5498Sa.json' );
        this.addTaxDoc( taxDataTax5498Sa );

        if ( quantity < 6 ) return;

        const taxDataTaxW2G: TaxData = require( '../data/TaxW2G.json' );
        this.addTaxDoc( taxDataTaxW2G );

        // this.saveTaxDataToStorage( );

    }

    updateTaxDocHubItemNumber(
        itemSha: string,
        itemNumber: string
    ) {

        // const snappedData: string = JSON.stringify( taxData );

        // this.postSnappedData( snappedData );

        // this.trace( 'GlobalDataService addTaxDoc adding...' );

        this.logger.trace( 'GlobalDataService updateTaxDocHubItemNumber begin' );

        this.storage.updateTaxDocHubItemNumber(
            itemSha,
            itemNumber
        );

    }

    addTaxDocListItem(
        taxData: TaxData,
        taxDocListItem: TaxDocListItem
    ): number {

        this.logger.trace( 'GlobalDataService addTaxDocListItem begin' );

        if ( this.isADuplicate( taxDocListItem) ) {
            return -1;
        }

        // =====================================================
        // Store to local storage
        // =====================================================
        const indexNumber: number = this.storage.addTaxDoc( taxDocListItem );

        // =====================================================
        // Post to tax doc hub
        // =====================================================
        this.postSnappedData( taxData, taxDocListItem );

        return indexNumber;

    }

    addTaxDoc(
        taxData: TaxData
    ) {

        this.logger.trace( 'GlobalDataService addTaxDoc begin' );

        const taxDocListItem: TaxDocListItem
            = this.convertTaxDataToListItem( taxData );

        this.addTaxDocListItem( taxData, taxDocListItem );

    }

    isADuplicate(
        taxDocListItem: TaxDocListItem
    ): boolean {

        this.logger.trace( 'GlobalDataService isADuplicate begin' );

        const all: TaxDocListItem[] = this.getTaxDocs( );

        const filteredList: any[] = all.filter(
            ( item ) => {
                return item.sha === taxDocListItem.sha;
            }
        );

        if ( filteredList.length > 0 ) {
            this.logger.trace( 'GlobalDataService isADuplicate true' );
            return true;
        }

        this.logger.trace( 'GlobalDataService isADuplicate false' );
        return false;

    }

    convertTaxDataToListItem(
        taxData: TaxDataForQR
    ): TaxDocListItem {

        const taxDataExtractor: TaxDataExtractor = new TaxDataExtractor( );

        taxDataExtractor.extractForTaxData( taxData );

        const taxDocListItem: TaxDocListItem = new TaxDocListItem(
            taxDataExtractor.docId,
            taxDataExtractor.docType,
            taxDataExtractor.docNumber,
            taxDataExtractor.description,
            taxDataExtractor.imageUrl,
            taxDataExtractor.issuerName,
            taxData
        );

        // Note: This string is not preserved. Not re-creatable on server.
        const asString: string = JSON.stringify( taxData );
        taxDocListItem.sha = sha256( asString );

        return taxDocListItem;

    }

    // getSimpleItemsForDoc(
    //     doc: TaxDocListItem
    // ): Array<DescriptionValue> {
    //
    //     const simpleItems: Array<DescriptionValue> = new Array<DescriptionValue>();
    //
    //     simpleItems.push(
    //         {
    //             description: 'Box 1. Wages, tips, other...',
    //             value: `115000.00`
    //         }
    //     );
    //
    //     simpleItems.push(
    //         {
    //             description: 'Box 2. Federal income tax ...',
    //             value: '11500.00'
    //         }
    //     );
    //
    //     simpleItems.push(
    //         {
    //             description: 'Box 4. To be determined ...',
    //             value: '1500.00'
    //         }
    //     );
    //
    //     return simpleItems;
    //
    // }

    recordScannedText(
        text: string
    ): number {

        this.logger.trace( `Global recordScannedText` );

        // Keep copy
        this.lastScannedText = text;

        let taxData: TaxData;

        try {

            taxData = JSON.parse( text );

            const taxDocListItem: TaxDocListItem
                = this.convertTaxDataToListItem( taxData );

            if ( this.isADuplicate( taxDocListItem ) ) {
                this.logger.trace( 'GlobalDataService recordScannedText duplicate' );
                return this.SCAN_DUPLICATE;
            }

            // If valid document, add
            const indexNumber: number = this.addTaxDocListItem( taxData, taxDocListItem );

            return indexNumber;

        } catch ( e ) {

            this.logger.trace( `Global recordScannedText parse error` );

            this.logger.debugAny( e );

            return this.PARSE_ERROR;

        }

        return this.PARSE_OK;

    }

    isLastScannedTextValid( ): boolean {

        return true;

    }

    public postSnappedData(
        taxData: TaxData,
        taxDocListItem: TaxDocListItem
    ) {

        this.logger.trace( `Global postSnappedData` );

        const deviceId: string = this.getDeviceGuid( );

        const sha: string = taxDocListItem.sha;

        this.logger.debugJsonWithLabel( taxData, 'scanned data' );

        const targetUrl = `${this.apiRoot}/tax/qr?key=${this.apiKey}&deviceId=${deviceId}`;

        this.logger.debugWithLabel( 'targetUrl', targetUrl );

/*

SAMPLE RESPONSE AS OF 2019 09 11

{
  "metadataList": [
    {
      "taxDocumentid": "268630001",
      "sha", "........."
    }
  ]
}

 */

        this.doPostJson( targetUrl, taxData )
            .subscribe(
                ( obj ) => {

                    this.logger.trace( `Global postSnappedData response = OK` );
                    this.logger.debugAny( obj );

                    this.metadataList = obj.metadataList;

                    // Capture number
                    const itemNumber: string = obj.metadataList[0].taxDocumentid;
                    this.logger.dump( 'assignedId', itemNumber );

                    const itemSha: string = sha;
                    this.logger.dump( 'itemSha', itemSha );

                    // Attach number to document
                    this.updateTaxDocHubItemNumber( itemSha, itemNumber );

                    this.serverResponse = JSON.stringify( obj, null, 4 );

                    this.uploadDataSubject.next( obj );

                },
                ( error ) => {

                    this.logger.trace( `Global postSnappedData response = error` );

                    this.logger.log( `ERROR: ${targetUrl}` );

                    this.logger.debugAny( error );

                }

                );

    }

    // ===============================================================
    // Generic post of json to specified url
    // ===============================================================
    doPostJson(
        targetUrl: string,
        data: any
    ): Observable<any> {

        const httpOptions: any = {
            headers: new HttpHeaders(
                { 'Content-Type': 'application/json' }
            ),
            observe: 'body',
            responseType: 'json'
        };

        const httpObservable: Observable<any> = this.httpClient.post(
            targetUrl,
            data,
            httpOptions
        );

        return httpObservable;

    }

    // ===============================================================
    // Generic post of form data to specified url
    // ===============================================================
    doPostForm(
        targetUrl: string,
        data: HttpParams
    ): Observable<any> {

        const httpOptions: any = {
            headers: new HttpHeaders(
                { 'Content-Type': 'application/x-www-form-urlencoded' }
            ),
            observe: 'body',
            responseType: 'json'
        };

        const postObservable: Observable<any> = this.httpClient.post(
            targetUrl,
            data,
            httpOptions
        );

        return postObservable;
    }

    // ===============================================================
    // Generic GET of json from specified url
    // ===============================================================
    doGetJson(
        targetUrl: string
    ): Observable<any> {

        const httpOptions: any = {
            observe: 'body',
            responseType: 'json'
        };

        const httpObservable: Observable<any> = this.httpClient.get<any>(
            targetUrl,
            httpOptions
        );

        return httpObservable;

    }

    // ===============================================================
    // Generic GET of text from specified url
    // ===============================================================
    doGetPlainText(
        targetUrl: string
    ): Observable<any> {

        const httpOptions: any = {
            observe: 'body',
            responseType: 'text'
        };

        const httpObservable: Observable<any> = this.httpClient.get<any>(
            targetUrl,
            httpOptions
        );

        return httpObservable;

    }

}

// Consider this approach for global vars
const API = 'https://www.site.com';

