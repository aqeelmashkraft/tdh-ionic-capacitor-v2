/**
 * FDX V4.0
 * Financial Data Exchange V4.0 API
 *
 * The version of the OpenAPI document: 4.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { NameAddress } from './nameAddress';
import { NameAddressPhone } from './nameAddressPhone';


export interface Tax2439AllOf { 
    /**
     * ISO 8601 date tine with milliseconds in UTC time zone
     */
    fiscalYearBegin?: string;
    /**
     * ISO 8601 date tine with milliseconds in UTC time zone
     */
    fiscalYearEnd?: string;
    ricOrReitNameAddress?: NameAddressPhone;
    /**
     * Identification number of RIC or REIT
     */
    ricOrReitTin?: string;
    shareholderNameAddress?: NameAddress;
    /**
     * Shareholder\'s identifying number
     */
    shareholderTin?: string;
    /**
     * Box 1a, Total undistributed long-term capital gains
     */
    undistributedLongTermCapitalGains?: number;
    /**
     * Box 1b, Unrecaptured section 1250 gain
     */
    unrecaptured1250Gain?: number;
    /**
     * Box 1c, Section 1202 gain
     */
    section1202Gain?: number;
    /**
     * Box 1d, Collectibles (28%) gain
     */
    collectiblesGain?: number;
    /**
     * Box 2, Tax paid by the RIC or REIT on the box 1a gains
     */
    taxPaid?: number;
}

