
/**
 * Description and value pair used on doc.component.html
 */
export class DescriptionValue {

    /**
     * Description
     */
    description?: string;
    /**
     * Amount
     */
    value?: string;

}

