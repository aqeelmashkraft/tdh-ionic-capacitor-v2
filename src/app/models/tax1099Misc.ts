/**
 * FDX V4.0
 * Financial Data Exchange V4.0 API
 *
 * The version of the OpenAPI document: 4.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Tax1099MiscAllOf } from './tax1099MiscAllOf';
import { TaxFormType } from './taxFormType';
import { StateTaxWithholding } from './stateTaxWithholding';
import { NameAddress } from './nameAddress';
import { Tax } from './tax';
import { TaxLinks } from './taxLinks';
import { NameAddressPhone } from './nameAddressPhone';


/**
 * Miscellaneous Income
 */
export interface Tax1099Misc { 
    /**
     * Year for which taxes are being paid
     */
    taxYear?: number;
    /**
     * true or false
     */
    corrected?: boolean;
    accountId?: string;
    taxFormId?: string;
    /**
     * ISO 8601 date tine with milliseconds in UTC time zone
     */
    taxFormDate?: string;
    /**
     * Description of the tax document
     */
    description?: string;
    /**
     * Additional explanation text or content for taxpayer or preparer or IRS about the tax document
     */
    additionalInformation?: string;
    taxFormType?: TaxFormType;
    links?: TaxLinks;
    payerNameAddress?: NameAddressPhone;
    /**
     * Payer\'s identification number
     */
    payerTin?: string;
    /**
     * Recipient\'s identification number
     */
    recipientTin?: string;
    recipientNameAddress?: NameAddress;
    /**
     * Account number
     */
    accountNumber?: string;
    /**
     * true or false
     */
    foreignAccountTaxCompliance?: boolean;
    /**
     * Box 1, Rents
     */
    rents?: number;
    /**
     * Box 2, Royalties
     */
    royalties?: number;
    /**
     * Box 3, Other income
     */
    otherIncome?: number;
    /**
     * Box 4, Federal income tax withheld
     */
    federalTaxWithheld?: number;
    /**
     * Box 5, Fishing boat proceeds
     */
    fishingBoatProceeds?: number;
    /**
     * Box 6, Medical and health care payments
     */
    medicalHealthPayment?: number;
    /**
     * Box 7, Nonemployee compensation
     */
    nonEmployeeCompensation?: number;
    /**
     * Box 8, Substitute payments in lieu of dividends or interest
     */
    substitutePayments?: number;
    /**
     * true or false
     */
    payerDirectSales?: boolean;
    /**
     * Box 10, Crop insurance proceeds
     */
    cropInsurance?: number;
    /**
     * Box 13, Excess golden parachute payments
     */
    excessGolden?: number;
    /**
     * Box 14, Gross proceeds paid to an attorney
     */
    grossAttorney?: number;
    /**
     * Box 15a, Section 409A deferrals
     */
    section409ADeferrals?: number;
    /**
     * Box 15b, Section 409A income
     */
    section409AIncome?: number;
    /**
     * State tax withholding
     */
    stateTaxWithholding?: Array<StateTaxWithholding>;
}

