import { TaxDataForQR } from './taxDataForQR';

export class TaxDocListItem {

    // SHA 256
    public sha: string;

    constructor(

        // Tax Doc Hub id number assigned by data store
        public docId: string,

        // Tax Doc class e.g. Tax1098
        public id: string,

        // e.g. 1098
        public docNumber: string,

        // e.g. Mortgage interest
        public description: string,

        // Image for background purposes
        public imageUrl: string,

        // Payer, etc name for recognition purposes
        public issuerName: string,

        // Data
        public taxData: TaxDataForQR

    ) {

    }

}
