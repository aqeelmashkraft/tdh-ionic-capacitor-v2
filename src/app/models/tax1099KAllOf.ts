/**
 * FDX V4.0
 * Financial Data Exchange V4.0 API
 *
 * The version of the OpenAPI document: 4.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { MonthAmount } from './monthAmount';
import { StateTaxWithholding } from './stateTaxWithholding';
import { NameAddress } from './nameAddress';
import { TelephoneNumberPlusExtension } from './telephoneNumberPlusExtension';
import { NameAddressPhone } from './nameAddressPhone';


export interface Tax1099KAllOf { 
    filerNameAddress?: NameAddressPhone;
    /**
     * true or false
     */
    paymentSettlementEntity?: boolean;
    /**
     * true or false
     */
    electronicPaymentFacilitator?: boolean;
    /**
     * true or false
     */
    paymentCard?: boolean;
    /**
     * true or false
     */
    thirdPartyNetwork?: boolean;
    payeeNameAddress?: NameAddress;
    /**
     * PSE\'s name   
     */
    pseName?: string;
    /**
     * Account number
     */
    accountNumber?: string;
    /**
     * FILER\'S TIN
     */
    filerTin?: string;
    /**
     * PAYEE\'S taxpayer identification no.
     */
    payeeFederalId?: string;
    /**
     * Box 1a, Gross amount of payment card/third party network transactions
     */
    grossAmount?: number;
    /**
     * Box 1b, Card Not Present Transactions
     */
    cardNotPresent?: number;
    /**
     * Box 2, Merchant category code
     */
    merchantCategoryCode?: string;
    /**
     * Box 3, Number of purchase transactions
     */
    numberOfTransactions?: number;
    /**
     * Box 4, Federal income tax withheld
     */
    federalTaxWithheld?: number;
    /**
     * Box 5, Monthly amounts
     */
    monthAmounts?: Array<MonthAmount>;
    /**
     * State tax withholding
     */
    stateTaxWithholding?: Array<StateTaxWithholding>;
    psePhone?: TelephoneNumberPlusExtension;
}

